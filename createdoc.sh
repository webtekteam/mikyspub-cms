#!/bin/bash
PHPDOC=$(which phpdoc)
PHPDOCMD="$PWD/vendor/gianiaz/phpdoc-md/bin/phpdocmd";
TEMPDIR="$PWD/var/tmp/docs";
DOCDIR="$PWD/wiki";
TPLDIR="$PWD/app/Resources/views/docs/";
[[ -z "${param// }" ]]
if [[ ! $PHPDOC = *[!\ ]* ]]; then
  echo "Il binario phpdoc non è presente nel PATH, scarica l'ultima versione stabile da qui: https://github.com/phpDocumentor/phpDocumentor2/releases in formato phar, poi copia il file in /usr/local/bin/phpdoc e dai i permessi 755"
  exit;
fi
if [ ! -f $PHPDOCMD ]; then
   echo "File $PHPDOCMD non trovato, hai installato con composer il package evert/phpdoc-md ?"
   exit;
fi
if [ ! -f $TEMPDIR ]; then
   mkdir -p $TEMPDIR;
fi;

$PHPDOC -q -d src -t $TEMPDIR --template="xml" && echo -e "\e[32m\xE2\x9C\x94 PHPDoc eseguito.\e[39m" && $PHPDOCMD --tpldir $TPLDIR $TEMPDIR/structure.xml $DOCDIR  && echo -e "\e[32m\xE2\x9C\x94 Phpdocmd eseguito\e[39m" && echo "Eseguo commit + push" &&  cd $DOCDIR && git add . && git commit -m "Aggiornamento documentazione" -a && git push && echo -e "\e[32m\xE2\x9C\x94 Terminato\e[39m"


