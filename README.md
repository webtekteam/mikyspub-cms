#WEBTEK CMS
I migliori auguri

##Premessa
- Tutti i progetti devono essere forkati dal master di webtekcms
- La repository deve essere chiamata [nome-progetto]-cms

##Setup e installazione
- Clona la repository
- Esegui composer install
- Dall'interno della cartella /web, esegui npm install
- Copia il file /app/config/config_dev.yml.dist nel file /app/config/config_dev.yml
- All'interno del file /app/config/config_dev.yml, imposta l'username e la password di [mailtrap](https://mailtrap.io/signin)
- All'interno del file /app/config/parameters.yml, impostare i parametri di connessione al db: database_host, database_port, database_name, database_user, database_password
- Eseguire, tramite bin/console, i comandi do:da:cr, do:sc:cr e do:fi:lo per creare il database di default
- Eseguire, tramite bin/console, il comando ass:ins (con l'opzione --symlink se si è in ambiente UNIX). Questo comando permette di installare i vari asset contenuti nei bundle del cms, inclusi i file js necessari per il suo funzionamento
- Eseguire, tramite bin/console, il comando s:r per far partire il server di sviluppo di Symfony; di default, questo si aprirà su localhost:8000

##Istruzioni per la collaborazione
- Non è possibile pushare direttamente sul master: per aggiunte/modifiche è necessario creare un branch e successivamente aprire una pull request; **importante modificare adeguatamente il file CHANGELOG.md**

##Bug conosciuti
- Tanti, la lista verrà aggiornata man mano che se ne definiranno