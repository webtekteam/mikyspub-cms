<?php
// 28/04/17, 8.28
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Tests\AppartamentiBundle\Tests\Controller;


use Tests\AppBundle\Base\BaseController;

class FeatureController extends BaseController
{

    public function setUpCrudController()
    {

        $this->addUrl('list', '/admin/feature');
        $this->addUrl('listJson', '/admin/feature/json');
        $this->addUrl('delete', '/admin/feature/delete/{id}');
        $this->addUrl('delete', '/admin/feature/delete/{id}/force');
        $this->addUrl('restore', '/admin/feature/restore/{id}');
        $this->addUrl('new', '/admin/feature/new');
        $this->addUrl('edit', '/admin/feature/edit/{id}');
        $this->addUrl('list', '/admin/feature/sort');

    }

}