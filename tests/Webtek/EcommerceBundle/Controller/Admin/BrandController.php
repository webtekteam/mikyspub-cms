<?php
// 27/04/17, 10.11
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Tests\AppartamentiBundle\Tests\Controller;

use Tests\AppBundle\Base\BaseController;

class BrandController extends BaseController
{

    public function setUpCrudController()
    {

        $this->addUrl('list', '/admin/group_attribute');
        $this->addUrl('listJson', '/admin/group_attribute/json');
        $this->addUrl('delete', '/admin/group_attribute/delete/{id}');
        $this->addUrl('delete', '/admin/group_attribute/delete/{id}/force');
        $this->addUrl('restore', '/admin/group_attribute/restore/{id}');
        $this->addUrl('new', '/admin/group_attribute/new');
        $this->addUrl('edit', '/admin/group_attribute/edit/{id}');

    }

}