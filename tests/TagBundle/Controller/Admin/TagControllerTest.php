<?php
// 22/04/17, 10.26
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Tests\TagBundle\Tests\Controller;


use Tests\AppBundle\Base\BaseController;

class TagControllerTest extends BaseController
{

    public function setUpCrudController()
    {

        $this->addUrl('list', '/admin/tag');
        $this->addUrl('listJson', '/admin/tag/json');
        $this->addUrl('delete', '/admin/tag/delete/{id}');
        $this->addUrl('delete', '/admin/tag/delete/{id}/force');
        $this->addUrl('restore', '/admin/tag/restore/{id}');
        $this->addUrl('new', '/admin/tag/new');
        $this->addUrl('edit', '/admin/tag/edit/{id}');
        $this->addUrl('autocomplete', '/admin/tag/autocomplete-json');

    }

}