<?php
// 23/11/17, 16.54
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Behat\AppBundle\Admin\Page;

use SensioLabs\Behat\PageObjectExtension\PageObject\Page;

class LoginPage extends BasePage
{

    protected $path = '/admin/login';

    public function iFillInTheAuthData($username, $password)
    {

        $this->findField('Username')->setValue($username);
        $this->findField('Password')->setValue($password);
        $this->findXpathDataAttr('login-button')->press();

    }

}