<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Behat\AppBundle\Admin\Page;

use Behat\Mink\Session;

class PanelPage extends BasePage
{
    public function waitForXpathToAppear($name)
    {
        $this->waitFor(
            0.5,
            function () use ($name) {
                return $this->findXpathDataAttr($name);
            }
        );
    }

    public function waitDataTable()
    {
        $this->waitFor(
            2,
            function () {
                return count($this->findXpathDataAttr('datatable')->find('css', 'tr.ajaxloaded'));
            }
        );
    }

    public function findRowByText($rowText)
    {
        $table = $this->findXpathDataAttr('datatable');
        $selector = sprintf('tbody > tr:contains("%s")', $rowText);
        $row = $table->find('css', $selector);
        assertNotNull($row, 'Riga con label "' . $rowText . '" non trovata');

        return $row;
    }

    public function editRow($rowText)
    {
        $row = $this->findRowByText($rowText);
        $editLink = $row->find('css', '.edit');
        assertNotNull($editLink, 'Non ho trovato il link di modifica per la riga con testo "' . $rowText . '"');
        $editLink->click();
    }

    public function deleteRow($rowText)
    {
        $row = $this->findRowByText($rowText);
        $deleteLink = $row->find('css', '.btn-delete');
        assertNotNull($deleteLink, 'Non ho trovato il link di cancellazione per la riga con testo "' . $rowText . '"');
        $deleteLink->click();
    }

    public function restoreRow($rowText)
    {
        $row = $this->findRowByText($rowText);
        $restoreLink = $row->find('css', '.btn-restore');
        assertNotNull($restoreLink, 'Non ho trovato il link di restore per la riga con testo "' . $rowText . '"');
        $restoreLink->click();
    }

    public function waitConfirmation($session)
    {
        $session->wait(2000, '$("[data-name=modal-confirm-ok]:visible").length');
    }

    public function confirmModal($session)
    {
        $modal = $this->findXpathDataAttr('modal');
        $okButton = $this->findXpathDataAttr('modal-confirm-ok', 'name', $modal);
        $okButton->press();
        $session->wait(2000, '$(".modal-backdrop").length == 0');
    }

    public function fillXpathDataAttr($label, $value, $element = 'name')
    {
        $element = $this->findXpathDataAttr($label, $element);
        $element->setValue($value);
    }

    public function selectXpathDataAttr($label, $value, $element = 'name')
    {
        $element = $this->findXpathDataAttr($label, $element);
        $element->selectOption($value);
    }

    public function dragToTop($label, Session $session)
    {
        // elemento da spostare
        $element = $this->findXpathDataAttr($label);
        // destinazione
        $sortable = $this->findXpathDataAttr('sortable');
        $script = '$(\'#%s\').simulate("drag", {dy: ($(\'#%s\').offset().top-$(\'#%s\').offset().top)});';
        $script = sprintf(
            $script,
            $element->getAttribute('id'),
            $sortable->getAttribute('id'),
            $element->getAttribute('id')
        );
        $session->evaluateScript($script);
    }

    public function fillDataTableSearch($query)
    {
        $dataTableWrapper = $this->find('css', '#datatable_filter');
        $search = $dataTableWrapper->find('css', '.form-control');
        assertNotNull($search, 'Campo di ricerca non trovato');
        $search->setValue($query);
    }

    public function clickOnNamedIcon($icon, $rowText)
    {
        $row = $this->findRowByText($rowText);
        $icona = $row->find('css', '.' . $icon);
        assertNotNull($icon, 'Non ho trovato l\'icona ' . $icon . ' nella riga con testo "' . $rowText . '"');
        $icona->click();
    }

    public function getImageSRC($label)
    {
        $element = $this->findXpathDataAttr($label);

        return $element->getAttribute('src');
    }

    public function pressXPathButton($label)
    {
        $button = $this->findXpathDataAttr($label);
        $button->press();
    }
}
