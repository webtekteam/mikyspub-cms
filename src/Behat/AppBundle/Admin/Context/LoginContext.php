<?php
// 23/11/17, 17.00
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Behat\AppBundle\Admin\Context;

use Behat\AppBundle\Admin\Page\LoginPage;
use Behat\AppBundle\Traits\Behat;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use SensioLabs\Behat\PageObjectExtension\PageObject\Page;

class LoginContext extends RawMinkContext implements Context
{

    use KernelDictionary;
    use Behat;

    /**
     * @var LoginPage
     */
    private $loginPage;

    /**
     * LoginContext constructor.
     */
    public function __construct(LoginPage $loginPage)
    {

        $this->loginPage = $loginPage;
    }

    /**
     * @Given I fill the login form with :username and :password
     */
    public function iFillTheLoginFormWithAnd($username, $password)
    {

        $this->loginPage->iFillInTheAuthData($username, $password);

    }

}