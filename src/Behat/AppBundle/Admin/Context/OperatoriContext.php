<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace Behat\AppBundle\Admin\Context;

use Behat\AppBundle\Admin\Page\OperatoriPage;
use Behat\AppBundle\Traits\Behat;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;

/**
 * Class OperatoriContext.
 */
class OperatoriContext extends RawMinkContext implements Context
{
    use KernelDictionary;
    use Behat;
    /**
     * @var OperatoriPage
     */
    private $operatoriPage;

    /**
     * OperatoriContext constructor.
     *
     * @param OperatoriPage $operatoriPage
     */
    public function __construct(OperatoriPage $operatoriPage)
    {
        $this->operatoriPage = $operatoriPage;
    }

    /**
     * @BeforeScenario
     */
    public function clearData()
    {
        $em = $this->getEntityManager();
        $User = $em->getRepository('AppBundle:User')->findOneBy(['email' => 'mario@verdi.it']);
        if ($User) {
            $em->remove($User);
        }
        $em->flush();
    }
}
