<?php
// 24/11/17, 16.46
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Behat\AppBundle\Traits;

use Psr\Container\ContainerInterface;

trait Behat
{

    protected function getEntityManager()
    {

        /**
         * @var $container ContainerInterface
         */
        $container = $this->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');

        return $em;

    }

}