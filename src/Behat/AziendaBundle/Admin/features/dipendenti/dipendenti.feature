Feature: Dipendenti
  In order to manage dipendenti
  As an admin user
  I need to be able to access to all crud actions

  Background:
    Given there is an admin user "admin" with password "admin"
    Given I am on the homepage
    And I fill the login form with "admin" and "admin"
    And I am on "/admin/"
    And I open "side-menu-azienda" menu
    And I wait for "side-menu-dipendenti" to appear
    And I open "side-menu-dipendenti" menu

  @javascript
  Scenario: List dipendenti
    Given I wait for table to load
    Then I should see "Gestione Dipendenti: Elenco elementi"

  @javascript
  Scenario: Create new anagrafica
    Given I follow "Nuovo"
    Then I should see "Gestione Dipendenti: Nuovo"
    And I fill in data "nome" with "Giovanni"
    And I fill in data "cognome" with "Lenoci"
    And I select "No" from data "is-enabled"
    And I fill in data "email" with "gianiaz+1@gmail.com"
    And I fill in data "telefono" with "0342212121"
    And I fill in data "password" with "webtek"
    And I fill in data "repeat-password" with "webtek"
    And I scrollo to "saveform"
    And I press element "saveform"
    Then I should see "Dipendente"
    And I should see "creato"
