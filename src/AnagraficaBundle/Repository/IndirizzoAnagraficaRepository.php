<?php
// 21/04/17, 17.03
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AnagraficaBundle\Repository;


use Doctrine\ORM\EntityRepository;

class IndirizzoAnagraficaRepository extends EntityRepository
{

    public function unsetDefault($defaultId, $anagrafica)
    {

        $qb = $this->createQueryBuilder('addres');
        $qb->update('AnagraficaBundle:IndirizzoAnagrafica', 'i')
            ->set('i.isDefault', '0')
            ->where('i.id != :defaultId')
            ->andWhere('i.anagrafica = :anagrafica')
            ->setParameter('anagrafica', $anagrafica)
            ->setParameter('defaultId', $defaultId);

        $qb->getQuery()->execute();

    }
}