<?php
// 13/04/17, 11.34
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AnagraficaBundle\Repository;


use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AnagraficaBundle\Entity\Anagrafica;

class AnagraficaRepository extends EntityRepository
{

    function areProductsInWishlist(Anagrafica $anagrafica, $products)
    {
        $prodottiWishlist = $anagrafica->getWishlistProducts();
        $ids = [];
        foreach ($prodottiWishlist as $p) {
            $ids[] = $p->getId();
        }
        foreach ($products as $prod) {
            if (in_array($prod->getId(), $ids)) {
                $prod->setInWishlist(1);
            } else {
                $prod->setInWishlist(0);
            }
        }
        return $products;
    }

    function areProductsInWishlistArray(Anagrafica $anagrafica, $products)
    {
        $prodottiWishlist = $anagrafica->getWishlistProducts();
        $ids = [];
        foreach ($prodottiWishlist as $p) {
            $ids[] = $p->getId();
        }
        foreach ($products as $prod) {
            if (in_array($prod[0]->getId(), $ids)) {
                $prod[0]->setInWishlist(1);
            } else {
                $prod[0]->setInWishlist(0);
            }
        }
        return $products;
    }


    function isProductInWishlist($product)
    {
        return $this->createQueryBuilder('anagrafica')
            ->join('anagrafica.wishlistProducts', 'products')
            ->andWhere('products = :product')
            ->setParameter("product", $product)->getQuery()->getOneOrNullResult();
    }

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('anagrafica')
            ->andWhere('anagrafica.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function getByUser(User $user)
    {

        return $this->createQueryBuilder('anagrafica')
            ->andWhere('anagrafica.user = :user')
            ->setParameter('user', $user)
            ->getQuery()->getSingleResult();
    }


}