<?php
// 02/01/17, 15.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AziendaBundle\Entity;

use AppBundle\Entity\Attachment;
use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 * @ORM\Table(name="dipendenti_attachments")
 */
class DipendenteAttachment extends Attachment
{

    use Loggable;
    /**
     * @ORM\ManyToOne(targetEntity="AziendaBundle\Entity\Dipendente", inversedBy="attachments")
     */
    private $dipendente;

    /**
     * @return mixed
     */
    public function getParent()
    {

        return $this->dipendente;
    }

    /**
     * @param mixed $product
     */
    public function setParent($dipendente)
    {

        $this->token = '';
        $this->parent_id = $dipendente->getId();
        $this->dipendente = $dipendente;
    }

}