<?php
// 29/06/17, 11.48
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AziendaBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Form\TypeExtension\BetterVichUploaderTypeExtension;
use AziendaBundle\Entity\Department;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepartmentForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('telefono', TextType::class, ['label' => 'sedi.labels.telefono', 'required' => false]);
        $builder->add('email', TextType::class, ['label' => 'sedi.labels.email', 'required' => false]);
        $builder->add(
            'fotoFile',
            BetterVichUploaderTypeExtension::class,
            [
                'label' => 'sedi.labels.foto',
                'required' => false,
                'allow_delete' => true,
                'download_uri' => true,
                'image_uri' => false,
            ]
        );

        $fields = [
            'titolo' => [
                'label' => 'sedi.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            'link' => [
                'label' => 'sedi.labels.link',
                'required' => false,
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'label' => false,
                'required_locales' => array_keys($options['langs']),
                'empty_data' => function (FormInterface $form) {

                    return new \Doctrine\Common\Collections\ArrayCollection();
                },
            ]

        );

        $builder->add(
            'orari',
            CollectionType::class,
            [
                'entry_type' => OrarioForm::class,
                'by_reference' => false,
                'allow_add' => true,
                'required' => false,
                'allow_delete' => true,
                'allow_extra_fields' => true,
                'label' => false,
                'entry_options' => ['label' => false],
            ]
        );

    }


    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Department::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
                'allow_extra_fields' => true,
            ]
        );
    }

}