<?php
// 27/06/17, 17.57
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AziendaBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AziendaBundle\Entity\Dipendente;
use AziendaBundle\Entity\Sede;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DipendenteForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('uuid', HiddenType::class, ['data' => Uuid::uuid1()->__toString()]);
        $builder->add('listImg', FileType::class);
        $builder->add('listImgData', HiddenType::class);
        $builder->add('listImgAlt', TextType::class, []);
        $builder->add('listImgDelete', HiddenType::class, []);
        $builder->add('nome', TextType::class, ['label' => 'dipendenti.labels.nome']);
        $builder->add('cognome', TextType::class, ['label' => 'dipendenti.labels.cognome']);
        $builder->add('email', TextType::class, ['label' => 'dipendenti.labels.email', 'required' => false]);
        $builder->add('telefono', TextType::class, ['label' => 'dipendenti.labels.telefono', 'required' => false]);
        $builder->add('facebook', TextType::class, ['label' => 'dipendenti.labels.facebook', 'required' => false]);
        $builder->add('twitter', TextType::class, ['label' => 'dipendenti.labels.twitter', 'required' => false]);
        $builder->add('linkedin', TextType::class, ['label' => 'dipendenti.labels.linkedin', 'required' => false]);
        $builder->add('googleplus', TextType::class, ['label' => 'dipendenti.labels.googleplus', 'required' => false]);
        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $fields = [
            'titolo' => [
                'label' => 'dipendenti.labels.titolo',
                'required' => false,
            ],
            'mansione' => [
                'label' => 'dipendenti.labels.mansione',
                'required' => false,
            ],
            'descrizione' => [
                'label' => 'dipendenti.labels.descrizione',
                'required' => false,
                'attr' => ['class' => 'ck'],
            ],
        ];
        $builder->add(
            'sedi',
            EntityType::class,
            [
                'class' => Sede::class,
                'multiple' => true,
                'expanded' => true,
                'by_reference' => false,
            ],
            ['label' => 'dipendenti.labels.sedi']
        );
        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Dipendente::class,
                'error_bubbling' => true,
                'allow_extra_fields' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }

}