<?php
// 28/06/17, 15.43
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AziendaBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SedeRepository extends EntityRepository
{

    function findAllNotDeleted($onlyActive = false)
    {

        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.deletedAt is NULL');

        if ($onlyActive) {
            $qb->andWhere('s.isEnabled = 1');
        };

        return $qb->getQuery()
            ->execute();
    }

    function findBySlug($slug, $locale = 'it')
    {

        $qb = $this->createQueryBuilder('sede')
            ->leftJoin(
                'AziendaBundle\Entity\SedeTranslation',
                'st',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'st.translatable = sede.id AND st.locale = :locale'
            )->setParameter('locale', $locale)
            ->andWhere('st.slug = :searchTerm')
            ->setParameter('searchTerm', $slug)
            ->andWhere('sede.deletedAt is NULL');

        return $qb->getQuery()->getOneOrNullResult();

    }

}