<?php
// 27/06/17, 17.54
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AziendaBundle\Repository;


use Doctrine\ORM\EntityRepository;

class DipendenteRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('d')
            ->andWhere('d.deletedAt is NULL')
            ->orderBy('d.sort', 'asc')
            ->getQuery()
            ->execute();
    }

}