<?php
// 19/04/17, 12.07
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ProvinciaRepository extends EntityRepository
{

    public function findByNation($nazione, $locale = 'it')
    {

        $qb = $this->createQueryBuilder('p')
            ->leftJoin(
                'GeoBundle\Entity\ProvinciaTranslation',
                'pt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = p.id AND pt.locale = :locale'
            )->setParameter('locale', $locale)
            ->where('p.nazione = :nazione')
            ->setParameter('nazione', $nazione)->orderBy('pt.nome', 'ASC');

        return $qb->getQuery()->getResult();

    }

}