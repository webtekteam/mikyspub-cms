<?php
// 19/04/17, 8.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GeoBundle\Entity\Comune;
use GeoBundle\Entity\ComuneTranslation;
use GeoBundle\Entity\Nazioni;
use GeoBundle\Entity\Provincia;
use GeoBundle\Entity\ProvinciaTranslation;
use GeoBundle\Entity\Regione;
use GeoBundle\Entity\RegioneTranslation;
use Symfony\Component\Intl\Intl;

class LoadGeo implements FixtureInterface
{

    private $csv;

    private $csvFilePath;

    private $manager;

    private $regioni = [];
    private $province = [];
    private $comuni = [];

    private $mappingRegioni = [];
    private $mappingProvince = [];

    public function load(ObjectManager $manager)
    {

        $this->manager = $manager;

        $this->csvFilePath = './src/GeoBundle/DataFixtures/ORM/Elenco-codici-statistici-e-denominazioni-al-24_03_2017.csv';

        $this->loadCSV();

        $this->scriviRegioni();

        $this->scriviProvince();

        $this->scriviComuni();

        $this->loadNations();


    }

    private function loadNations()
    {

        $countriesCodes = array_keys(Intl::getRegionBundle()->getCountryNames());

        foreach ($countriesCodes as $countryCode) {
            $country = new Nazioni();
            $country->setCountryCode($countryCode);
            $this->manager->persist($country);
        }

        $this->manager->flush();
    }


    private function loadCSV()
    {

        $riga = 0;

        if (($handle = fopen($this->csvFilePath, "r")) !== false) {


            while (($data = fgetcsv($handle, 1000, ";")) !== false) {

                if ($riga) {

                    if (!isset($this->regioni[intval($data[0])]) && $data[9]) {
                        $record = [];
                        $record['id'] = intval($data[0]);
                        $record['nome'] = trim($data[9]);

                        $this->regioni[$record['id']] = $record;

                    }

                    if (!isset($this->province[intval($data[2])]) && $data[9]) {

                        $record = [];
                        $record['id'] = intval($data[2]);
                        $record['sigla'] = trim($data[13]);
                        $record['regione'] = intval($data[0]);
                        $record['nome'] = trim($data[10]);
                        if ($record['nome'] == '-') {
                            $record['nome'] = trim($data[11]);
                        }

                        if ($record['nome'] && $record['sigla']) {
                            $this->province[$record['id']] = $record;
                        }

                    }

                    if (!isset($this->comuni[intval($data[4])]) && $data[5]) {

                        $record = [];
                        $record['id'] = intval($data[4]);
                        $record['regione'] = intval($data[0]);
                        $record['provincia'] = intval($data[2]);
                        $record['nome'] = trim($data[5]);

                        if ($record['nome']) {
                            $this->comuni[$record['id']] = $record;
                        }

                    }
                }

                $riga++;
            }

            fclose($handle);
        }

    }

    private function scriviRegioni()
    {

        foreach ($this->regioni as $regione) {

            $Regione = new Regione();
            $Regione->setId($regione['id']);
            $Regione->setNazione('IT');

            $Translation = new RegioneTranslation();
            $Translation->setTranslatable($Regione);
            $Translation->setNome(utf8_encode($regione['nome']));
            $Translation->setLocale('it');

            $this->manager->persist($Regione);
            $this->manager->persist($Translation);

            $this->mappingRegioni[$regione['id']] = $Regione;


        }

        $this->manager->flush();

    }

    private function scriviProvince()
    {

        foreach ($this->province as $provincia) {

            $Provincia = new Provincia();
            $Provincia->setId($provincia['id']);
            $Provincia->setSigla($provincia['sigla']);
            $Provincia->setRegione($this->mappingRegioni[$provincia['regione']]);
            $Provincia->setNazione('IT');


            $Translation = new ProvinciaTranslation();
            $Translation->setTranslatable($Provincia);
            $Translation->setNome(utf8_encode($provincia['nome']));
            $Translation->setLocale('it');

            $this->manager->persist($Provincia);
            $this->manager->persist($Translation);

            $this->mappingProvince[$provincia['id']] = $Provincia;

        }

        $this->manager->flush();

    }

    private function scriviComuni()
    {

        foreach ($this->comuni as $comune) {

            $Comune = new Comune();
            $Comune->setId($comune['id']);
            $Comune->setRegione($this->mappingRegioni[$comune['regione']]);
            $Comune->setProvincia($this->mappingProvince[$comune['provincia']]);
            $Comune->setNazione('IT');

            $Translation = new ComuneTranslation();
            $Translation->setTranslatable($Comune);
            $Translation->setNome(utf8_encode($comune['nome']));
            $Translation->setLocale('it');

            $this->manager->persist($Comune);
            $this->manager->persist($Translation);

        }

        $this->manager->flush();

    }

}