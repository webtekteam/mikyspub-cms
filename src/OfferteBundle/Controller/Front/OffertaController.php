<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Controller\Front;

use OfferteBundle\Entity\Offerta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class OffertaController extends Controller
{
    /**
     * @Route("/offerte/{slug}", defaults={"_locale"="it"}, name="offerta_it")
     * @Route("/{_locale}/offerte/{slug}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl"}, defaults={"_locale"="it"}, name="offerta")
     */
    public function readAction(Request $request)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $OffertaTranslation = $em->getRepository('OfferteBundle:OffertaTranslation')
            ->findOneBy(['slug' => $request->get('slug')]);

        if ($OffertaTranslation) {
            $seo_manager = $this->get('app.seo-manager');

            /**
             * @var Offerta $offerta
             */
            $offerta = $OffertaTranslation->getTranslatable();

            if ($offerta && $offerta->getIsEnabled()) {
                $TemplateLoader = $this->get('app.template_loader');
                $Languages = $this->get('app.languages');

                $AdditionalData = [];
                $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
                $AdditionalData['Entity'] = $offerta;

                $meta = $seo_manager->run($offerta);
                $meta = array_merge($meta, $this->get('app.open_graph')
                    ->generateOpenGraphData($offerta));

                $AdditionalData['META'] = $meta;

                $twigs = $TemplateLoader->getTwigs('offerta', $Languages->getActivePublicLanguages(), $AdditionalData);

                return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $meta]);
            }
        }

        throw new NotFoundHttpException('Pagina non trovata');
    }
}
