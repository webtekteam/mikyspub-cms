<?php
// 15/05/17, 16.06
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\PrezzoMarketPlaceRepository")
 * @ORM\Table(name="prezzo_marketplace")
 */
class PrezzoMarketPlace
{

    use ORMBehaviours\Timestampable\Timestampable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal",  precision=12, scale=6, nullable=true)
     */
    private $valore;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    private $valoreIvato;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\MarketPlace")
     * @ORM\JoinColumn(name="marketplace_id", referencedColumnName="id")
     */
    private $marketPlace;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $prodotto;

    public function __toString()
    {

        return (string)$this->getId();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getValore()
    {

        return $this->valore;
    }

    /**
     * @param mixed $valore
     */
    public function setValore($valore)
    {

        $this->valore = $valore;
    }

    /**
     * @return mixed
     */
    public function getMarketPlace()
    {

        return $this->marketPlace;
    }

    /**
     * @param mixed $marketPlace
     */
    public function setMarketPlace($marketPlace)
    {

        $this->marketPlace = $marketPlace;
    }


    /**
     * @return mixed
     */
    public function getProdotto()
    {

        return $this->prodotto;
    }

    /**
     * @param mixed $prodotto
     */
    public function setProdotto($prodotto)
    {

        $this->prodotto = $prodotto;
    }

    /**
     * @return mixed
     */
    public function getValoreIvato()
    {

        return $this->valoreIvato;
    }

    /**
     * @param mixed $valoreIvato
     */
    public function setValoreIvato($valoreIvato)
    {

        $this->valoreIvato = $valoreIvato;
    }

    /**
     * @return mixed
     */
    public function getisEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }


}