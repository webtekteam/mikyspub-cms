<?php
// 02/05/17, 17.48
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\DeliveryPricesRepository")
 * @ORM\Table(name="delivery_prices")
 */
class DeliveryPrices
{

    use Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $tutte;

    /**
     * @ORM\ManyToMany(targetEntity="GeoBundle\Entity\Nazioni")
     * @ORM\JoinTable(name="delivery_prices_has_nazioni",
     * inverseJoinColumns={@ORM\JoinColumn(name="nazioni_id", referencedColumnName="countryCode")},
     * joinColumns={@ORM\JoinColumn(name="delivery_prices_id", referencedColumnName="id")})
     */
    private $nations;

    /**
     * @ORM\Column(type="decimal",precision=7, scale=4)
     */
    private $cost;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $sogliaMin;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $sogliaMax;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Delivery", inversedBy="prices")
     */
    private $delivery;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTutte()
    {

        return $this->tutte;
    }

    /**
     * @param mixed $tutte
     */
    public function setTutte($tutte)
    {

        $this->tutte = $tutte;
    }

    /**
     * @return mixed
     */
    public function getNations()
    {

        return $this->nations;
    }

    /**
     * @param mixed $nations
     */
    public function setNations($nations)
    {

        $this->nations = $nations;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {

        return $this->cost;
    }

    /**
     * @param mixed $cost
     */
    public function setCost($cost)
    {

        $this->cost = $cost;
    }

    /**
     * @return mixed
     */
    public function getSogliaMin()
    {

        return $this->sogliaMin;
    }

    /**
     * @param mixed $sogliaMin
     */
    public function setSogliaMin($sogliaMin)
    {

        $this->sogliaMin = $sogliaMin;
    }

    /**
     * @return mixed
     */
    public function getSogliaMax()
    {

        return $this->sogliaMax;
    }

    /**
     * @param mixed $sogliaMax
     */
    public function setSogliaMax($sogliaMax)
    {

        $this->sogliaMax = $sogliaMax;
    }

    /**
     * @return mixed
     */
    public function getDelivery()
    {

        return $this->delivery;
    }

    /**
     * @param mixed $delivery
     */
    public function setDelivery($delivery)
    {

        $this->delivery = $delivery;
    }


}