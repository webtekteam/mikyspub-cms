<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\ProductTypeRepository")
 * @ORM\Table(name="product_type")
 * @UniqueEntity(fields={"nome"})
 */
class ProductType
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $nome;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\GroupAttribute")
     * @ORM\JoinTable(name="product_type_has_group_attribute")
     */
    private $gruppiAttributi;

    /**
     * ProductType constructor.
     */
    public function __construct()
    {

        $this->gruppiAttributi = new ArrayCollection();

    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    public function __toString()
    {

        return (string)$this->getNome();
    }

    /**
     * @return mixed
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getGruppiAttributi()
    {

        return $this->gruppiAttributi;
    }

    /**
     * @param mixed $gruppiAttributi
     */
    public function setGruppiAttributi($gruppiAttributi)
    {

        $this->gruppiAttributi = $gruppiAttributi;
    }


}
