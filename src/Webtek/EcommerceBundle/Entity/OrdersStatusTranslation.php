<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="orders_status_translations")
 */
class OrdersStatusTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titolo;


    /**
     * @ORM\Column(type="text", length=255)
     */
    private $messaggioEmail;

    /**
     * @return mixed
     */
    public function getMessaggioEmail()
    {

        return $this->messaggioEmail;
    }

    /**
     * @param mixed $messaggioEmail
     */
    public function setMessaggioEmail($messaggioEmail)
    {

        $this->messaggioEmail = $messaggioEmail;
    }

    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

}

