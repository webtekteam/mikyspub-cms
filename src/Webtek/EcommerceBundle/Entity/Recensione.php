<?php
/**
 * Created by PhpStorm.
 * User: gabricom
 * Date: 24/08/17
 * Time: 11.33
 */

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="recensioni")
 */
class Recensione
{

    use ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\SoftDeletable\SoftDeletable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AnagraficaBundle\Entity\Anagrafica", inversedBy="recensioni")
     */
    private $anagrafica;


    /**
     * @Assert\Type(
     *     type="numeric"
     * )
     * @ORM\Column(type="float")
     */
    private $valore;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotBlank()
     */
    private $messaggio;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Product", inversedBy="recensioni")
     */
    private $prodotto;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAnagrafica()
    {

        return $this->anagrafica;
    }

    /**
     * @param mixed $nome
     */
    public function setAnagrafica($anagrafica)
    {

        $this->anagrafica = $anagrafica;
    }


    /**
     * @return mixed
     */
    public function getMessaggio()
    {

        return $this->messaggio;
    }

    /**
     * @param mixed $messaggio
     */
    public function setMessaggio($messaggio)
    {

        $this->messaggio = $messaggio;
    }

    /**
     * @return mixed
     */
    public function getValore()
    {

        return $this->valore;
    }

    /**
     * @param mixed $parent
     */
    public function setValore($valore)
    {

        $this->valore = $valore;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getProdotto()
    {

        return $this->prodotto;
    }

    /**
     * @param mixed $news
     */
    public function setProdotto($prodotto)
    {

        $this->prodotto = $prodotto;
    }


}