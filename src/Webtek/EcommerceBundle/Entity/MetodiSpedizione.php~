<?php
// 06/02/17, 11.02
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>

namespace Webtek\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Webtek\EcommerceBundle\Traits\Price as Price;
/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\MetodiSpedizioneRepository")
 * @ORM\Table(name="ecommerce_shipment_methods")
 */
class MetodiSpedizione {
  use ORMBehaviours\SoftDeletable\SoftDeletable,
      Price,
      ORMBehaviours\Timestampable\Timestampable;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="boolean")
   */
  private $isEnabled;

  /**
   * @ORM\Column(type="string", nullable=false)
   * @Assert\NotBlank()
   */
  private $nome;

  /**
   * @ORM\Column(type="string", nullable=false)
   * @Assert\NotBlank()
   */
  private $tipo;


    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $descrizione;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $nomeFatturazione;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Nazioni" , inversedBy="metodi_spedizione")
     * @ORM\JoinTable(name="ecommerce_shipments_nations",
     *      joinColumns={@ORM\JoinColumn(name="shipment_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="countryCode_ref", referencedColumnName="countryCode")}
     *      )
     */
    private $nazioni;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="ScontiSpedizionePerPrezzo", mappedBy="metodo_spedizione")
     */
    private $sconti_prezzo;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $indirizzoFatturazione;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $capFatturazione;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $cittaFatturazione;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $provinciaFatturazione;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $nazioneFatturazione;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $telefonoFatturazione;

    /**
     * @return mixed
     */
    public function getValiditaNazioni()
    {
        return $this->validitaNazioni;
    }

    /**
     * @param mixed $validitaNazioni
     */
    public function setValiditaNazioni($validitaNazioni)
    {
        $this->validitaNazioni = $validitaNazioni;
    }

    /**
     * @ORM\Column(type="boolean",nullable=false)
     */
    private $validitaNazioni;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return MetodiSpedizione
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return MetodiSpedizione
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return MetodiSpedizione
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return MetodiSpedizione
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * Set prezzo
     *
     * @param string $prezzo
     *
     * @return MetodiSpedizione
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return string
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }

    /**
     * Set nomeFatturazione
     *
     * @param string $nomeFatturazione
     *
     * @return MetodiSpedizione
     */
    public function setNomeFatturazione($nomeFatturazione)
    {
        $this->nomeFatturazione = $nomeFatturazione;

        return $this;
    }

    /**
     * Get nomeFatturazione
     *
     * @return string
     */
    public function getNomeFatturazione()
    {
        return $this->nomeFatturazione;
    }

    /**
     * Set indirizzoFatturazione
     *
     * @param string $indirizzoFatturazione
     *
     * @return MetodiSpedizione
     */
    public function setIndirizzoFatturazione($indirizzoFatturazione)
    {
        $this->indirizzoFatturazione = $indirizzoFatturazione;

        return $this;
    }

    /**
     * Get indirizzoFatturazione
     *
     * @return string
     */
    public function getIndirizzoFatturazione()
    {
        return $this->indirizzoFatturazione;
    }

    /**
     * Set capFatturazione
     *
     * @param string $capFatturazione
     *
     * @return MetodiSpedizione
     */
    public function setCapFatturazione($capFatturazione)
    {
        $this->capFatturazione = $capFatturazione;

        return $this;
    }

    /**
     * Get capFatturazione
     *
     * @return string
     */
    public function getCapFatturazione()
    {
        return $this->capFatturazione;
    }

    /**
     * Set cittaFatturazione
     *
     * @param string $cittaFatturazione
     *
     * @return MetodiSpedizione
     */
    public function setCittaFatturazione($cittaFatturazione)
    {
        $this->cittaFatturazione = $cittaFatturazione;

        return $this;
    }

    /**
     * Get cittaFatturazione
     *
     * @return string
     */
    public function getCittaFatturazione()
    {
        return $this->cittaFatturazione;
    }

    /**
     * Set provinciaFatturazione
     *
     * @param string $provinciaFatturazione
     *
     * @return MetodiSpedizione
     */
    public function setProvinciaFatturazione($provinciaFatturazione)
    {
        $this->provinciaFatturazione = $provinciaFatturazione;

        return $this;
    }

    /**
     * Get provinciaFatturazione
     *
     * @return string
     */
    public function getProvinciaFatturazione()
    {
        return $this->provinciaFatturazione;
    }

    /**
     * Set nazioneFatturazione
     *
     * @param string $nazioneFatturazione
     *
     * @return MetodiSpedizione
     */
    public function setNazioneFatturazione($nazioneFatturazione)
    {
        $this->nazioneFatturazione = $nazioneFatturazione;

        return $this;
    }

    /**
     * Get nazioneFatturazione
     *
     * @return string
     */
    public function getNazioneFatturazione()
    {
        return $this->nazioneFatturazione;
    }

    /**
     * Set telefonoFatturazione
     *
     * @param string $telefonoFatturazione
     *
     * @return MetodiSpedizione
     */
    public function setTelefonoFatturazione($telefonoFatturazione)
    {
        $this->telefonoFatturazione = $telefonoFatturazione;

        return $this;
    }

    /**
     * Get telefonoFatturazione
     *
     * @return string
     */
    public function getTelefonoFatturazione()
    {
        return $this->telefonoFatturazione;
    }



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nazioni = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add nazioni
     *
     * @param \Webtek\EcommerceBundle\Entity\Nazioni $nazioni
     *
     * @return MetodiSpedizione
     */
    public function addNazioni(\Webtek\EcommerceBundle\Entity\Nazioni $nazioni)
    {
        $this->nazioni[] = $nazioni;

        return $this;
    }

    /**
     * Remove nazioni
     *
     * @param \Webtek\EcommerceBundle\Entity\Nazioni $nazioni
     */
    public function removeNazioni(\Webtek\EcommerceBundle\Entity\Nazioni $nazioni)
    {
        $this->nazioni->removeElement($nazioni);
    }

    /**
     * Get nazioni
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNazioni()
    {
        return $this->nazioni;
    }

    public function setNazioni($nazioni){
        $this->nazioni = $nazioni;
        return $this;
    }

    /**
     * Add scontiPrezzo
     *
     * @param \Webtek\EcommerceBundle\Entity\ScontiSpedizionePerPrezzo $scontiPrezzo
     *
     * @return MetodiSpedizione
     */
    public function addScontiPrezzo(\Webtek\EcommerceBundle\Entity\ScontiSpedizionePerPrezzo $scontiPrezzo)
    {
        $this->sconti_prezzo[] = $scontiPrezzo;

        return $this;
    }

    /**
     * Remove scontiPrezzo
     *
     * @param \Webtek\EcommerceBundle\Entity\ScontiSpedizionePerPrezzo $scontiPrezzo
     */
    public function removeScontiPrezzo(\Webtek\EcommerceBundle\Entity\ScontiSpedizionePerPrezzo $scontiPrezzo)
    {
        $this->sconti_prezzo->removeElement($scontiPrezzo);
    }

    /**
     * Get scontiPrezzo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScontiPrezzo()
    {
        return $this->sconti_prezzo;
    }
}
