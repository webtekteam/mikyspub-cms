<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\GroupFeatureRepository")
 * @ORM\Table(name="group_feature")
 * @UniqueEntity(fields={"chiave"})
 */
class GroupFeature
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\Feature", mappedBy="featureGroup")
     */
    private $features;

    /**
     * @ORM\Column(type="string")
     */
    private $chiave;

    public function __construct()
    {

        $this->features = new ArrayCollection();

    }

    public function __toString()
    {

        return (string)$this->translate()->getTitolo();
    }

    /**
     * @return mixed
     */
    public function getChiave()
    {

        return $this->chiave;
    }

    /**
     * @param mixed $chiave
     */
    public function setChiave($chiave)
    {

        $this->chiave = $chiave;
    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getFeatures()
    {

        return $this->features;
    }

    /**
     * @param mixed $features
     */
    public function setFeatures($features)
    {

        $this->features = $features;
    }


}
