<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\ListinoRepository")
 * @ORM\EntityListeners("Webtek\EcommerceBundle\EntityListener\ListinoListener")
 * @ORM\Table(name="listino")
 */
class Listino
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDefault;

    /**
     * Listino constructor.
     * @param $id
     */
    public function __construct()
    {

        $this->setIsDefault(0);
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }


    public function __toString()
    {

        return (string)$this->getId();
    }


    /**
     * @return mixed
     */
    public function getisDefault()
    {

        return $this->isDefault;
    }

    /**
     * @param mixed $isDefault
     */
    public function setIsDefault($isDefault)
    {

        $this->isDefault = $isDefault;
    }


}
