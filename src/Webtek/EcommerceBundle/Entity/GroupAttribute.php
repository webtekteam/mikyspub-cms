<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\GroupAttributeRepository")
 * @ORM\Table(name="group_attribute")
 */
class GroupAttribute
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\Attribute", mappedBy="attributeGroup")
     */
    private $attribute;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nomeAmazon;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    public function __toString()
    {

        return (string)$this->translate()->getTitolo();
    }

    /**
     * @return mixed
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {

        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getAttribute()
    {

        return $this->attribute;
    }

    /**
     * @param mixed $attribute
     */
    public function setAttribute($attribute)
    {

        $this->attribute = $attribute;
    }

    /**
     * @return mixed
     */
    public function getNomeAmazon()
    {

        return $this->nomeAmazon;
    }

    /**
     * @param mixed $nomeAmazon
     */
    public function setNomeAmazon($nomeAmazon)
    {

        $this->nomeAmazon = $nomeAmazon;
    }


}
