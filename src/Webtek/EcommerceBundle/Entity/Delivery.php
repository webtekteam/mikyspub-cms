<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\DeliveryRepository")
 * @ORM\Table(name="delivery")
 */
class Delivery
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string")
     */
    private $ragioneSociale;
    /**
     * @ORM\Column(type="string")
     */
    private $indirizzo;
    /**
     * @ORM\Column(type="string")
     */
    private $cap;
    /**
     * @ORM\Column(type="string")
     */
    private $comune;
    /**
     * @ORM\Column(type="string")
     */
    private $provincia;
    /**
     * @ORM\Column(type="string")
     */
    private $nazione;
    /**
     * @ORM\Column(type="string")
     */
    private $telefono;

    /**
     * @ORM\ManyToMany(targetEntity="MetodiPagamento" , inversedBy="deliveryMethods")
     * @ORM\JoinTable(name="ecommerce_payments_for_shipment",
     *      joinColumns={@ORM\JoinColumn(name="shipment_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="payment_id", referencedColumnName="id")}
     *      )
     */
    private $metodi_pagamento_validi;


    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Tax")
     * @ORM\JoinColumn(name="tax_id", referencedColumnName="id")
     */
    private $tax;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\DeliveryPrices", mappedBy="delivery",cascade={"persist"} )
     */
    private $prices;

    /**
     * Delivery constructor.
     */
    public function __construct()
    {

        $this->prices = new ArrayCollection();
        $this->metodi_pagamento_validi = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getMetodiPagamentoValidi()
    {

        return $this->metodi_pagamento_validi;
    }

    /**
     * @param mixed $metodi_pagamento_validi
     */
    public function setMetodiPagamentoValidi($metodi_pagamento_validi)
    {

        $this->metodi_pagamento_validi = $metodi_pagamento_validi;
    }


    public function addMetodiPagamentoValidi(MetodiPagamento $pag)
    {

        $this->metodi_pagamento_validi[] = $pag;

        return $this;
    }

    public function removeMetodiPagamentoValidi(MetodiPagamento $pag)
    {

        $this->metodi_pagamento_validi->removeElement($pag);
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    public function __toString()
    {

        return (string)$this->translate()->getNome();
    }

    /**
     * @return mixed
     */
    public function getRagioneSociale()
    {

        return $this->ragioneSociale;
    }

    /**
     * @param mixed $ragioneSociale
     */
    public function setRagioneSociale($ragioneSociale)
    {

        $this->ragioneSociale = $ragioneSociale;
    }

    /**
     * @return mixed
     */
    public function getIndirizzo()
    {

        return $this->indirizzo;
    }

    /**
     * @param mixed $indirizzo
     */
    public function setIndirizzo($indirizzo)
    {

        $this->indirizzo = $indirizzo;
    }

    /**
     * @return mixed
     */
    public function getCap()
    {

        return $this->cap;
    }

    /**
     * @param mixed $cap
     */
    public function setCap($cap)
    {

        $this->cap = $cap;
    }

    /**
     * @return mixed
     */
    public function getComune()
    {

        return $this->comune;
    }

    /**
     * @param mixed $comune
     */
    public function setComune($comune)
    {

        $this->comune = $comune;
    }

    /**
     * @return mixed
     */
    public function getProvincia()
    {

        return $this->provincia;
    }

    /**
     * @param mixed $provincia
     */
    public function setProvincia($provincia)
    {

        $this->provincia = $provincia;
    }

    /**
     * @return mixed
     */
    public function getNazione()
    {

        return $this->nazione;
    }

    /**
     * @param mixed $nazione
     */
    public function setNazione($nazione)
    {

        $this->nazione = $nazione;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {

        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {

        $this->telefono = $telefono;
    }


    public function addPrice(DeliveryPrices $deliveryPrice)
    {

        $deliveryPrice->setDelivery($this);
        $this->prices->add($deliveryPrice);
    }

    public function removePrice(DeliveryPrices $deliveryPrice)
    {

        $this->prices->removeElement($deliveryPrice);
    }

    /**
     * @return mixed
     */
    public function getPrices()
    {

        return $this->prices;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {

        return $this->tax;
    }

    /**
     * @param mixed $tax
     */
    public function setTax($tax)
    {

        $this->tax = $tax;
    }


}
