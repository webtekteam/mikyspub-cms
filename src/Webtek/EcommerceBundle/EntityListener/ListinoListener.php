<?php
// 05/05/17, 12.49
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\EntityListener;


use Doctrine\ORM\Event\PreUpdateEventArgs;
use Webtek\EcommerceBundle\Entity\Listino;

class ListinoListener
{

    /**
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(Listino $listino, PreUpdateEventArgs $event)
    {

        $em = $event->getEntityManager();

        $changed = $event->getEntityChangeSet();
        if ($changed) {
            if (isset($changed['isDefault'])) {
                $em->getRepository('WebtekEcommerceBundle:Listino')->setDefault($event->getEntity()->getId());
            }
        }

    }

}