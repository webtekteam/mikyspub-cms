<?php

namespace Webtek\EcommerceBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Webtek\EcommerceBundle\Entity\Order;
use Webtek\EcommerceBundle\Entity\Product;
use Symfony\Component\HttpFoundation\JsonResponse;
use Webtek\EcommerceBundle\Event\WishlistAddEvent;

/**
 * @Security("has_role('ROLE_ANAGRAFICA')")
 */
class AccountController extends Controller
{


    /**
     * @Route("/account/edit",  defaults={"_locale"="it"}, name="account_edit_it")
     * @Route("/{_locale}/account/edit", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="account_edit")
     */
    public function editAction(Request $request)
    {

        $Languages = $this->get('app.languages');
        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];
        $AdditionalData = [];
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);
        $pathManager = $this->get('app.path_manager');

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
            $META['alternate'][$key] = $pathManager->generateUrl(
                'account_edit',
                [],
                $sigla
            );
        }
        $AdditionalData['META'] = $META;
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');
        $twigs = $TemplateLoader->getTwigs(
            'account_edit',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );
        $META = $MetaManager->merge($twigs, $META);
        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];
            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'account-edit',
                    $META,
                    $metaVars
                );
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'account-edit', $META, $metaVars);
            }
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $authorizationChecker = $this->container->get('security.authorization_checker');

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }

    /**
     * @Route("/account/address",  defaults={"_locale"="it"}, name="account_address_it")
     * @Route("/{_locale}/account/address", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="account_address")
     */
    public function addressAction(Request $request)
    {

        $Languages = $this->get('app.languages');
        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];

        $AdditionalData = [];
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);
        $pathManager = $this->get('app.path_manager');

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
            $META['alternate'][$key] = $pathManager->generateUrl(
                'account_address',
                [],
                $sigla
            );
        }

        $AdditionalData['META'] = $META;
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');
        $twigs = $TemplateLoader->getTwigs(
            'account_edit',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );
        $META = $MetaManager->merge($twigs, $META);
        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];
            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'account-address',
                    $META,
                    $metaVars
                );
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'account-address', $META, $metaVars);
            }
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $authorizationChecker = $this->container->get('security.authorization_checker');

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }

    /**
     * @Route("/account/wishlist/addRemove/{product}",  defaults={"_locale"="it"}, name="account_wishlist_toggle")
     */
    public function wishlistToggleAction(Request $request, Product $product)
    {

        $Anagrafica = $this->getDoctrine()->getRepository('AnagraficaBundle:Anagrafica')->getByUser($this->getUser());
        if ($this->getDoctrine()->getRepository('AnagraficaBundle:Anagrafica')->isProductInWishlist($product)) {
            $Anagrafica->removeWishlistProduct($product);
        } else {
            $event = new WishlistAddEvent($product);
            $this->container->get('event_dispatcher')->dispatch(WishlistAddEvent::NAME, $event);
            $Anagrafica->addWishlistProduct($product);
        }
        $this->getDoctrine()->getManager()->persist($product);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(1);
    }

    /**
     * @Route("/account/wishlist/add/{product}",  defaults={"_locale"="it"}, name="account_wishlist_add")
     */
    public function wishlistAddAction(Request $request, Product $product)
    {

        $Anagrafica = $this->getDoctrine()->getRepository('AnagraficaBundle:Anagrafica')->getByUser($this->getUser());
        $Anagrafica->addWishlistProduct($product);
        $this->getDoctrine()->getManager()->persist($product);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(1);
    }

    /**
     * @Route("/account/wishlist/remove/{product}",  defaults={"_locale"="it"}, name="account_wishlist_remove")
     */
    public function wishlistRemoveAction(Request $request, Product $product)
    {

        $Anagrafica = $this->getDoctrine()->getRepository('AnagraficaBundle:Anagrafica')->getByUser($this->getUser());
        $Anagrafica->removeWishlistProduct($product);
        $this->getDoctrine()->getManager()->persist($product);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(1);
    }


    /**
     * @Route("/account/wishlist",  defaults={"_locale"="it"}, name="account_wishlist_it")
     * @Route("/{_locale}/account/wishlist", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="account_wishlist")
     */
    public function wishlistAction(Request $request)
    {

        $Languages = $this->get('app.languages');
        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];

        $AdditionalData = [];
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);
        $pathManager = $this->get('app.path_manager');

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
            $META['alternate'][$key] = $pathManager->generateUrl(
                'account_wishlist',
                [],
                $sigla
            );
        }
        $AdditionalData['META'] = $META;
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');
        $twigs = $TemplateLoader->getTwigs(
            'account_wishlist',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );
        $META = $MetaManager->merge($twigs, $META);
        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];
            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'account-wishlist',
                    $META,
                    $metaVars
                );
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'account-wishlist', $META, $metaVars);
            }
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $authorizationChecker = $this->container->get('security.authorization_checker');

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }

    /**
     * @Route("/account/orders",  defaults={"_locale"="it"}, name="account_orders_it")
     * @Route("/{_locale}/account/orders", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="account_orders")
     */
    public function ordersAction(Request $request)
    {

        $Languages = $this->get('app.languages');
        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];

        $AdditionalData = [];
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);
        $pathManager = $this->get('app.path_manager');

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
            $META['alternate'][$key] = $pathManager->generateUrl(
                'account_orders',
                [],
                $sigla
            );
        }
        $AdditionalData['META'] = $META;
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');
        $twigs = $TemplateLoader->getTwigs(
            'account_orders',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );
        $META = $MetaManager->merge($twigs, $META);
        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];
            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'account-orders',
                    $META,
                    $metaVars
                );
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'account-orders', $META, $metaVars);
            }
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $authorizationChecker = $this->container->get('security.authorization_checker');

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }

    /**
     * @Route("/account/order/{id}",  defaults={"_locale"="it"}, name="account_order_detail_it")
     * @Route("/{_locale}/account/order/{id}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="account_order_detail")
     */
    public function orderDetailAction(Request $request, Order $Order)
    {

        $user = $this->getUser();
        $anagrafica = $this->getDoctrine()->getRepository("AnagraficaBundle:Anagrafica")->getByUser($user);
        if ($anagrafica != $Order->getAnagraficaId()) {
            throw new UnauthorizedHttpException("L'ordine non appartiene all'utente");
        }
        $Languages = $this->get('app.languages');
        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];
        $AdditionalData = [];
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);
        $pathManager = $this->get('app.path_manager');

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
            $META['alternate'][$key] = $pathManager->generateUrl(
                'account_order_detail',
                [
                    "id" => $Order->getId()
                ],
                $sigla
            );
        }
        $AdditionalData['META'] = $META;
        $AdditionalData['Entity'] = $Order;
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');
        $twigs = $TemplateLoader->getTwigs(
            'account_order_detail',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );
        $META = $MetaManager->merge($twigs, $META);
        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];
            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'account-order-detail',
                    $META,
                    $metaVars
                );
                $META['description'] = $META['description'] . " n. " . $Order->getId();
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'account-order-detail', $META, $metaVars);
                $META['title'] = $META['title'] . " n. " . $Order->getId();
            }
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $authorizationChecker = $this->container->get('security.authorization_checker');

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }
}
