<?php
// 24/05/17, 9.02
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Controller\Front;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Webtek\EcommerceBundle\Service\MetaManager;
use Webtek\EcommerceBundle\Entity\Category;
use Symfony\Component\HttpFoundation\JsonResponse;
use Webtek\EcommerceBundle\Entity\CategoryTranslation;
use Webtek\EcommerceBundle\Entity\Product;

class CategoryController extends Controller
{


    /**
     * @Route("/productscategory-json/{id}/{page}"))
     */
    public function infiniteLoadingAction(Request $request, Category $id, $page)
    {

        $Category = $id;
        $em = $this->getDoctrine()->getManager();
        $data = [];
        $NewsHeader = [];
        $next = false;
        $prev = false;
        $order = $request->get("orderby");
        $dir = $request->get("dir");
        $products = $em->getRepository("Webtek\\EcommerceBundle\\Entity\\Product")->getProdottiAttiviCategory(
            $Category,
            $order,
            $dir,
            $request->getLocale()
        );
        $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);
        $paginator = $this->container->get('knp_paginator');
        $pagination = $paginator->paginate(
            $products,
            $page,
            $this->getParameter('generali')['elementi_pagina_paginatore']
        );
        $data = [];
        $data = $this->get("app.webtek_ecommerce.services.product_helper")->productsEntityParser($pagination, $request);
        $return = [];
        $return['result'] = true;
        $return['data'] = $data;
        if (isset($page)) {
            $return['page'] = $page;
        }

        return new JsonResponse($return);


    }


    /**
     * @Route("/prodotti/{slug}", defaults={"_locale"="it", "page"="1"}, requirements={"slug"=".+"}, name="categoria_ecommerce_it")
     * @Route("/{_locale}/prodotti/{slug}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es","slug"=".+"}, defaults={"_locale"="it", "page"="1"}, name="categoria_ecommerce")
     */
    public function readAction(Request $request, $slug, $_locale)
    {

        $categoryHelper = $this->get('app.webtek_ecommerce.services.category_helper');
        $Languages = $this->get('app.languages');
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');
        if ($slug) {

            $Category = $categoryHelper->retrieve($slug, $_locale);
            if ($Category) {

                $AdditionalData = [];
                $AdditionalData['Entity'] = $Category;
                $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
                $META = [];
                $META['title'] = $Category->translate($request->getLocale())->getMetaTitle();
                $META['description'] = $Category->translate($request->getLocale())->getMetaDescription();
                $META['robots'] = $Category->getRobots();
                $META['alternate'] = [];
                $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
                $linguePreferite = explode(',', $linguePreferite);
                foreach ($AdditionalData['langs'] as $sigla => $estesa) {
                    $key = $sigla;
                    if ($key == $linguePreferite[0]) {
                        $key = 'x-default';
                    }
                    $META['alternate'][$key] = $categoryHelper->generaUrl($Category, $sigla, [], true);
                }
                $AdditionalData['META'] = $META;
                $twigs = $TemplateLoader->getTwigs(
                    $Category->getTemplate(),
                    $Languages->getActivePublicLanguages(),
                    $AdditionalData
                );
                $META = $MetaManager->merge($twigs, $META);
                if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

                    $metaVars = ['{NOME}' => $Category->translate()->getNome()];
                    if (!isset($META['description']) || !$META['description']) {
                        $META = $MetaManager->manageDefaults('description', 'categorie-ecommerce', $META, $metaVars);
                    }
                    if (!isset($META['title']) || !$META['title']) {
                        $META = $MetaManager->manageDefaults('title', 'categorie-ecommerce', $META, $metaVars);
                    }
                }

                return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

            }

        }
        throw new NotFoundHttpException("Pagina non trovata");


    }

}