<?php
// 16/03/2017 , 10:46
// @author : Gabriele "gabricom" Colombera <gabricom-kun@live.it>

namespace Webtek\EcommerceBundle\Controller\Admin;

use Webtek\EcommerceBundle\Entity\MetodiPagamento;
use Webtek\EcommerceBundle\Form\MetodiPagamentoForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_PAYMENTS_METHODS')")
 */
class MetodiPagamentoController extends Controller
{

    /**
     * @Route("/metodi-pagamento-ecommerce", name="metodi-pagamento-ecommerce")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin//payments/list.html.twig');

    }

    /**
     * @Route("/metodi-pagamento-ecommerce/new", name="metodi-pagamento-ecommerce_new")
     */
    public function newAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(MetodiPagamentoForm::class, null, ['langs' => $langs]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $MetodoPagamento = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($MetodoPagamento);
                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Metodo di pagamento ' . $MetodoPagamento->translate()->getNome() . " " . $translator->trans(
                        'default.labels.creato'
                    )
                );

                return $this->redirectToRoute('metodi-pagamento-ecommerce');
            } else {
                $this->addFlash('error', 'Impossibile creare l\'elemento , controlla gli errori');
            }


        }

        return $this->render('@WebtekEcommerce/admin/payments/new.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/metodi-pagamento-ecommerce/toggle-enabled/{id}", name="metodi-pagamento-ecommerce_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, MetodiPagamento $metodiPagamento)
    {

        $em = $this->getDoctrine()->getManager();

        $translator = $this->get('translator');

        $elemento = $metodiPagamento->translate()->getNome();

        $flash = 'Metodo di pagamento ' . $elemento . ' ';

        if ($metodiPagamento->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $metodiPagamento->setIsEnabled(!$metodiPagamento->getIsEnabled());

        $em->persist($metodiPagamento);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('metodi-pagamento-ecommerce');


    }

    /**
     * @Route("/metodi-pagamento-ecommerce/toggle-debug/{id}", name="metodi-pagamento-ecommerce_toggle_debug")
     */
    public function toggleIsDebugAction(Request $request, MetodiPagamento $metodiPagamento)
    {

        $em = $this->getDoctrine()->getManager();

        $translator = $this->get('translator');

        $elemento = $metodiPagamento->translate()->getNome();

        $flash = 'Debug del Metodo di pagamento ' . $elemento . ' ';

        if ($metodiPagamento->getIsDebug()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $metodiPagamento->setIsDebug(!$metodiPagamento->getIsDebug());

        $em->persist($metodiPagamento);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('metodi-pagamento-ecommerce');


    }

    /**
     * @Route("/metodi-pagamento-ecommerce/edit/{id}", name="metodi-pagamento-ecommerce_edit")
     */
    public function editAction(Request $request, MetodiPagamento $metodiPagamento)
    {

        $em = $this->getDoctrine()->getManager();

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(MetodiPagamentoForm::class, $metodiPagamento, ['langs' => $langs]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($metodiPagamento);
                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Metodo Pagamento ' . $metodiPagamento->translate()->getNome() . ' ' . $translator->trans(
                        'default.labels.modificato'
                    )
                );

                return $this->redirectToRoute('metodi-pagamento-ecommerce');
            } else {
                $this->addFlash('error', 'Impossibile modificare l\'elemento , controlla gli errori');
            }


        }

        return $this->render('@WebtekEcommerce/admin//payments/new.html.twig', ['form' => $form->createView()]);

    }


    /**
     * @Route("/metodi-pagamento-ecommerce/json", name="metodi-pagamento-ecommerce_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $MetodiPagamento = $em->getRepository('Webtek\EcommerceBundle\Entity\MetodiPagamento')->findAll();


        $retData = [];

        $list = [];

        foreach ($MetodiPagamento as $MetodoPagamento) {
            /**
             * @var $MetodoPagamento MetodiPagamento
             */
            $record = [];
            $record['id'] = $MetodoPagamento->getId();
            $record['isEnabled'] = $MetodoPagamento->getIsEnabled();
            $record['isDebug'] = $MetodoPagamento->getIsDebug();
            $record['nome'] = $MetodoPagamento->translate()->getNome();
            $record['descrizione'] = $MetodoPagamento->translate()->getDescrizione();

            $list[] = $record;
        }

        $retData['data'] = $list;

        return new JsonResponse($retData);

    }


}
