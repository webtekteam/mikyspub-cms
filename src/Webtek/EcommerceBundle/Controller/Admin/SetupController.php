<?php
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\FormError;
use Webtek\EcommerceBundle\Entity\Setup;
use Webtek\EcommerceBundle\Form\SetupForm;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;

/**
 * @Route("/admin")
 */
class SetupController extends Controller
{


    /**
     * @Route("/setup-ecommerce", name="setup_ecommerce")
     * @Security("is_granted('ROLE_MANAGE_SETUP_ECOMMERCE')")
     */
    public function listAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $Setup = $this->getDoctrine()->getRepository('Webtek\EcommerceBundle\Entity\Setup')->findAll();
        if ($Setup) {
            $Setup = $Setup[0];
        } else {
            $Setup = new Setup();
        }
        $form = $this->createForm(SetupForm::class, $Setup);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $Setup = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($Setup);
                $em->flush();
                $this->addFlash('success', 'Impostazioni modificate con successo!');
            } else {
                $this->addFlash('error', 'Risolvi i problemi per poter modificare i parametri');
            }


        }

        return $this->render('@WebtekEcommerce/admin//setup/list.html.twig', ['setupForm' => $form->createView()]);


    }


}
