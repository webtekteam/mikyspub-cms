<?php
// 29/05/17, 12.24
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;


use AppBundle\DataFormatter\DataFormatter;
use Webtek\EcommerceBundle\Entity\Carrello;

class CarrelloFormatter extends DataFormatter
{

    var $Carrello = null;

    public function getData()
    {

        $data = [];

        if ($this->Carrello) {

            $data['Carrello'] = $this->Carrello;

            return $data;
        }


    }

    public function getCount()
    {

        $CarrelloHelper = $this->container->get('app.webtek_ecommerce.services.carrello_helper');
        return ["quantita" => $CarrelloHelper->getNumberOfItems($CarrelloHelper->getCarrello())];
    }

    public function extractData()
    {

        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Carrello) {

            $this->Carrello = $this->AdditionalData['Entity'];

        }
    }


}