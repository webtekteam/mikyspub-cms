<?php
// @author : gabricom

namespace Webtek\EcommerceBundle\DataFormatter;

use AppBundle\DataFormatter\DataFormatter;

class SetupFormatter extends DataFormatter
{


    public function getData()
    {

        // TODO: Implement getData() method.
    }

    public function getCondizioniVendita()
    {
        return ["condizioni_vendita" => $this->em->getRepository("WebtekEcommerceBundle:Setup")->findAll()[0]->getCondizioniVendita()];
    }

    public function getPrivacyPolicy()
    {
        return ["privacy_policy" => $this->em->getRepository("WebtekEcommerceBundle:Setup")->findAll()[0]->getPrivacyPolicy()];
    }

    public function extractData()
    {


    }


}