<?php

namespace Webtek\EcommerceBundle\Service;

use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Listino;

class ListinoHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ListinosHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Listinos = $this->entityManager->getRepository('WebtekEcommerceBundle:Listino')->findAll();
        } else {
            $Listinos = $this->entityManager->getRepository('WebtekEcommerceBundle:Listino')->findAllNotDeleted();
        }
        $records = [];
        foreach ($Listinos as $Listino) {


            /**
             * @var $Listino Listino;
             */
            $record = [];
            $record['id'] = $Listino->getId();
            $record['titolo'] = $Listino->translate()->getTitolo();
            $record['deleted'] = $Listino->isDeleted();
            $record['isDefault'] = $Listino->getisDefault();
            if ($record['isDefault']) {
                $record['cancellabile'] = 0;
            }
            $record['createdAt'] = $Listino->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Listino->getUpdatedAt()->format('d/m/Y H:i:s');
            $records[] = $record;
        }

        return $records;

    }

    public function getSimpleArray()
    {

        $Listinos = $this->entityManager->getRepository('WebtekEcommerceBundle:Listino')->findAllNotDeleted();
        $data = [];
        foreach ($Listinos as $Listino) {

            /**
             * @var $Listino Listino;
             */
            $data[$Listino->getId()] = $Listino->translate()->getTitolo();

        }

        return $data;

    }

    public function getDefault()
    {

        $Listino = $this->entityManager->getRepository('WebtekEcommerceBundle:Listino')->findOneBy(['isDefault' => 1]);

        return $Listino;

    }

}