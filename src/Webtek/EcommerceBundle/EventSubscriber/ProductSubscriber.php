<?php
// 18/09/17, 7.41
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\EventSubscriber;

use AnagraficaBundle\Entity\Anagrafica;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Event\AnagraficaSavedEvent;
use Webtek\EcommerceBundle\Event\ProductSavedEvent;
use Webtek\EcommerceBundle\Service\ProductHelper;

class ProductSubscriber implements EventSubscriberInterface
{

    /**
     * @var ProductHelper
     */
    private $productHelper;
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(ProductHelper $productHelper, EntityManager $entityManager)
    {

        $this->productHelper = $productHelper;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {

        $events = [
            ProductSavedEvent::NAME => 'onProductSaved',
        ];

        return $events;

    }

    public function onProductSaved(ProductSavedEvent $event)
    {

        /**
         * @var $Product Product
         */
        $Product = $event->getProduct();
        $Root = $this->entityManager->getRepository('WebtekEcommerceBundle:CategoryTranslation')->findOneBy(
            ['nome' => 'Radice']
        );
        if ($Root) {
            $Root = $Root->getTranslatable();
        }
        $Categories = $Product->getCategorie();
        $Categories->add($Root);
        foreach ($Categories as $category) {
            $this->productHelper->createSortEntry($Product, $category);
        }


    }

}