<?php
// 05/06/17, 12.36
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SceltaSpedizionieriForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->address = $options['address'];
        $this->totaleCarrello = $options['totaleCarrello'];

        $builder->add('address', HiddenType::class);

        $builder->add(
            'delivery',
            EntityType::class,
            [
                'label' => false,
                'expanded' => true,
                'class' => 'Webtek\EcommerceBundle\Entity\Delivery',
                'query_builder' => function ($er) use ($options) {

                    return $er->getByAddressAndCartQB($options['address'], $options['totaleCarrello']);

                },
            ]
        );


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'error_bubbling' => false,
                'locale' => 'en',
                'address' => false,
                'totaleCarrello' => false,
            ]
        );
    }

}