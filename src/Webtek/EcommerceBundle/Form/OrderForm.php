<?php
// 12/06/17, 11.02
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\Order;

class OrderForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'status',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\OrdersStatus',
                'label' => 'order.labels.stato_ordine',
                'required' => true,
            ]
        );

        $builder->add(
            'payment',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\MetodiPagamento',
                'label' => 'order.labels.metodo_pagamento',
                'required' => true,
            ]
        );

        $builder->add(
            'pagato',
            ChoiceType::class,
            [
                'label' => 'order.labels.pagato',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );


        $builder->add(
            'transactionId',
            TextType::class,
            [
                'label' => 'order.labels.transaction_id',
                'help' => 'order.help.transaction_id',
            ]
        );
        //$builder->add('payedAmount', TextType::class, ['label' => 'order.labels.payed_amount']);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Order::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );

    }


}