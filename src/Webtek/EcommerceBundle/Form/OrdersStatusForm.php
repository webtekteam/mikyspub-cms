<?php
// 07/06/17, 12.06
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\OrdersStatus;

class OrdersStatusForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'codice',
            TextType::class,
            ['label' => 'orders_status.labels.codice']
        );

        $fields = [
            'titolo' => [
                'label' => 'orders_status.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

    }


    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => OrdersStatus::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}