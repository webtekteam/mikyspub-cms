<?php

namespace Webtek\EcommerceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\UM;

class UMForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'label',
            TextType::class,
            [
                'label' => 'um.labels.label',
                'required' => true,
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => UM::class,
                'error_bubbling' => true,
            ]
        );
    }


}
