<?php
// 18/09/17, 7.34
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Webtek\EcommerceBundle\Entity\Carrello;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;

class CarrelloAddEvent extends Event
{

    const NAME = 'carrello.add';
    /**
     * @var VarianteProdotto
     */
    private $VarianteProdotto;
    /**
     * @var Product
     */
    private $Product;

    public function __construct(Product $Product, VarianteProdotto $VarianteProdotto = null)
    {

        $this->VarianteProdotto = $VarianteProdotto;
        $this->Product = $Product;

    }

    public function getProduct()
    {

        return $this->Product;

    }

    /**
     * @return VarianteProdotto
     */
    public function getVarianteProdotto()
    {

        return $this->VarianteProdotto;
    }


}