<?php

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmailTemplateRepository")
 * @ORM\Table(name="email_template")
 * @UniqueEntity("codice")
 */
class EmailTemplate
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $codice;

    /**
     * @ORM\Column(type="text")
     */
    private $variabili;

    public function __toString()
    {

        return (string)$this->translate()->getSubject();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCodice()
    {

        return $this->codice;
    }

    /**
     * @param mixed $codice
     */
    public function setCodice($codice)
    {

        $this->codice = $codice;
    }

    /**
     * @return mixed
     */
    public function getVariabili()
    {

        return $this->variabili;
    }

    /**
     * @param mixed $variabili
     */
    public function setVariabili($variabili)
    {

        $this->variabili = $variabili;
    }

}
