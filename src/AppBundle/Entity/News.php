<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 * @ORM\EntityListeners({"AppBundle\EntityListener\NewsListener"})
 * @ORM\Table(name="news")
 * @Vich\Uploadable()
 */
class News
{
    use Loggable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, ORMBehaviours\SoftDeletable\SoftDeletable;

    public $listImgData;

    public $headerImgData;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\NewsRows", mappedBy="news", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $newsRows;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\NewsAttachment", mappedBy="news", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $attachments;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publishAt;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\NewsHasCategory", mappedBy="news", cascade={"all"})
     */
    private $news_categories_association;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Commento", mappedBy="news", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $commenti;

    /**
     * @ORM\Column(type="boolean")
     */
    private $commentiEnabled;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    /**
     * @Vich\UploadableField(mapping="news", fileNameProperty="listImgFileName")
     */
    private $listImg;
    private $listImgDelete;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgAlt;

    /**
     * @Vich\UploadableField(mapping="news", fileNameProperty="headerImgFileName")
     */
    private $headerImg;
    private $headerImgDelete;

    private $uuid;

    private $categorieAggiuntive;

    public function __construct()
    {
        $this->newsRows = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->news_categories_association = new ArrayCollection();
        $this->commenti = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->getId();
    }

    public function addNewsCategoriesAssociation(NewsHasCategory $newsHasCategory)
    {
        $this->news_categories_association[] = $newsHasCategory;
    }

    public function removeNewsCategoriesAssociation(NewsCategory $newsCategory)
    {
        $this->news_categories_association = $this->news_categories_association->filter(
            function ($entry) use ($newsCategory) {
                return $entry->getCategory() != $newsCategory || 1 == $entry->getIsMain();
            }
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNewsRows()
    {
        return $this->newsRows;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param mixed $attachments
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Add newsRow.
     *
     * @param $newsRows
     *
     * @return News
     */
    public function addNewsRow(NewsRows $newsRows)
    {
        $newsRows->setNews($this);
        $this->newsRows[] = $newsRows;

        return $this;
    }

    /**
     * Remove newsRow.
     *
     * @param \AppBundle\Entity\NewsRows $newsRows
     */
    public function removeNewsRow(NewsRows $newsRows)
    {
        $this->newsRows->removeElement($newsRows);
    }

    public function getUploadDir()
    {
        return 'files/news/' . $this->getId() . '/';
    }

    /**
     * @return mixed
     */
    public function getPublishAt()
    {
        return $this->publishAt;
    }

    /**
     * @param mixed $publishAt
     */
    public function setPublishAt($publishAt)
    {
        $this->publishAt = $publishAt;
    }

    /**
     * @return NewsCategory
     */
    public function getPrimaryCategory()
    {
        $data = $this->news_categories_association->filter(
            function ($entry) {
                return 1 == $entry->getIsMain();
            }
        );
        if (!$data->isEmpty()) {
            return $data->last()->getCategory();
        }

        return null;
    }

    /**
     * @return NewsCategory[]
     */
    public function getCategorieAggiuntive()
    {
        //dump($this->news_categories_association);
        $data = $this->news_categories_association->filter(
            function ($entry) {
                return 0 == $entry->getIsMain();
            }
        );
        $return = [];
        foreach ($data as $entry) {
            /*
             * @var $entry NewsCategory
             */
            $return[] = $entry->getCategory();
        }

        return $return;
    }

    public function setPrimaryCategory(NewsCategory $newsCategory)
    {
        $newsCategoryAssociations = new NewsHasCategory();
        $newsCategoryAssociations->setCategory($newsCategory);
        $newsCategoryAssociations->setIsMain(true);
        $newsCategoryAssociations->setNews($this);
        if (!$this->news_categories_association->exists(
            function ($key, $entry) use ($newsCategory) {
                return 1 == $entry->getIsMain() && $entry->getCategory() == $newsCategory;
            }
        )) {
            $this->news_categories_association = $this->news_categories_association->filter(
                function ($entry) {
                    return 1 != $entry->getIsMain();
                }
            );
            $this->addNewsCategoriesAssociation($newsCategoryAssociations);
        }
        $this->addNewsCategoriesAssociation($newsCategoryAssociations);
    }

    public function getNewsCategoriesAssociation()
    {
        return $this->news_categories_association;
    }

    public function addCategorieAggiuntive(NewsCategory $newsCategory)
    {
        $newsCategoryAssociations = new NewsHasCategory();
        $newsCategoryAssociations->setCategory($newsCategory);
        $newsCategoryAssociations->setIsMain(false);
        $newsCategoryAssociations->setNews($this);
        $this->news_categories_association->add($newsCategoryAssociations);
    }

    public function setCategorieAggiuntive($newsCategories)
    {
        foreach (array_diff($this->getCategorieAggiuntive(), $newsCategories) as $category) {
            $this->removeNewsCategoriesAssociation($category);
        }
        foreach ($newsCategories as $newsCategory) {
            if (!$this->news_categories_association->exists(
                function ($key, $entry) use ($newsCategory) {
                    return $entry->getCategory() == $newsCategory;
                }
            )) {
                $this->addCategorieAggiuntive($newsCategory);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getCommenti()
    {
        return $this->commenti;
    }

    /**
     * @return mixed
     */
    public function getCommentiEnabled()
    {
        return $this->commentiEnabled;
    }

    /**
     * @param mixed $commentiEnabled
     */
    public function setCommentiEnabled($commentiEnabled)
    {
        $this->commentiEnabled = $commentiEnabled;
    }

    /** IMMAGINI **/

    /**
     * @return File
     */
    public function getListImg()
    {
        return $this->listImg;
    }

    public function setListImg($listImg = null)
    {
        $this->listImg = $listImg;
        if ($listImg) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getListImgFileName()
    {
        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {
        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {
        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {
        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {
        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {
        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {
        return $this->listImgData;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgFileName()
    {
        return $this->headerImgFileName;
    }

    /**
     * @param mixed $headerImgFileName
     */
    public function setHeaderImgFileName($headerImgFileName)
    {
        $this->headerImgFileName = $headerImgFileName;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgAlt()
    {
        return $this->headerImgAlt;
    }

    /**
     * @param mixed $headerImgAlt
     */
    public function setHeaderImgAlt($headerImgAlt)
    {
        $this->headerImgAlt = $headerImgAlt;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgDelete()
    {
        return $this->headerImgDelete;
    }

    /**
     * @param mixed $headerImgDelete
     */
    public function setHeaderImgDelete($headerImgDelete)
    {
        $this->headerImgDelete = $headerImgDelete;
    }

    /**
     * @return mixed
     */
    public function getHeaderImg()
    {
        return $this->headerImg;
    }

    /**
     * @param mixed $headerImg
     */
    public function setHeaderImg($headerImg)
    {
        $this->headerImg = $headerImg;
        if ($headerImg) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgData()
    {
        return $this->headerImgData;
    }
}
