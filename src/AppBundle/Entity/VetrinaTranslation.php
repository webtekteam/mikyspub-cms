<?php
// 09/01/17, 16.10
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @Gedmo\Loggable
 * @ORM\Table(name="vetrina_translations")
 */
class VetrinaTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=128)
     */
    private $titolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $testo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;

    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo()
    {

        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo)
    {

        $this->sottotitolo = $sottotitolo;
    }

    /**
     * @return mixed
     */
    public function getTesto()
    {

        return $this->testo;
    }

    /**
     * @param mixed $testo
     */
    public function setTesto($testo)
    {

        $this->testo = $testo;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {

        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {

        $this->url = $url;
    }

}