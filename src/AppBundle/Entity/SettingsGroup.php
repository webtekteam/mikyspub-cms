<?php
// 19/01/17, 15.31
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingsGroupRepository")
 * @ORM\Table(name="settings_groups")
 */
class SettingsGroup
{

    use ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\SoftDeletable\SoftDeletable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Settings", mappedBy="group")
     */
    private $settings;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {

        $this->name = $name;
    }

    function __toString()
    {

        return $this->getName();
    }


}