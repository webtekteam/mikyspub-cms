<?php
// 20/01/17, 10.24
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TemplateRepository")
 * @ORM\Table(name="template_config")
 */
class Template
{

    use ORMBehaviours\Timestampable\Timestampable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $template;

    /**
     * @ORM\Column(type="string")
     */
    private $view;

    /**
     * @ORM\Column(type="string")
     */
    private $module;

    /**
     * @ORM\Column(type="string")
     */
    private $category;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="string")
     */
    private $instance;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     */
    private $container;

    /**
     * @ORM\Column(type="json_array")
     */
    private $config;

    /**
     * @ORM\Column(type="string")
     */
    private $codice;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string")
     */
    private $layout;

    /**
     * @ORM\Column(type="string")
     */
    private $requiredRole;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {

        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {

        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getView()
    {

        return $this->view;
    }

    /**
     * @param mixed $view
     */
    public function setView($view)
    {

        $this->view = $view;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {

        return $this->module;
    }

    /**
     * @param mixed $module
     */
    public function setModule($module)
    {

        $this->module = $module;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {

        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {

        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getInstance()
    {

        return $this->instance;
    }

    /**
     * @param mixed $instance
     */
    public function setInstance($instance)
    {

        $this->instance = $instance;
    }

    /**
     * @return mixed
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {

        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {

        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {

        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {

        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {

        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getCodice()
    {

        return $this->codice;
    }

    /**
     * @param mixed $codice
     */
    public function setCodice($codice)
    {

        $this->codice = $codice;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {

        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout)
    {

        $this->layout = $layout;
    }

    /**
     * @return mixed
     */
    public function getRequiredRole()
    {

        return $this->requiredRole;
    }

    /**
     * @param mixed $requiredRole
     */
    public function setRequiredRole($requiredRole)
    {

        $this->requiredRole = $requiredRole;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {

        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {

        $this->category = $category;
    }


}