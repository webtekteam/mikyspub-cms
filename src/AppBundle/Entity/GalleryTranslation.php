<?php

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use AppBundle\Traits\Seo;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\EntityListeners({"AppBundle\EntityListener\GalleryTranslationListener"})
 * @ORM\Table(name="gallery_translations")
 */
class GalleryTranslation
{

    use ORMBehaviours\Translatable\Translation;
    use Seo, Loggable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titolo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $testo;


    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo()
    {

        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo)
    {

        $this->sottotitolo = $sottotitolo;
    }

    /**
     * @return mixed
     */
    public function getTesto()
    {

        return $this->testo;
    }

    /**
     * @param mixed $testo
     */
    public function setTesto($testo)
    {

        $this->testo = $testo;
    }


}

