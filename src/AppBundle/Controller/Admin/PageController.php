<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Page;
use AppBundle\Entity\PageRows;
use AppBundle\Entity\PageTranslation;
use AppBundle\Form\PageForm;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_PAGES')")
 */
class PageController extends Controller
{
    /**
     * @Route("/pages", name="pages_list")
     */
    public function listAction()
    {
        return $this->render('admin/pages/list.html.twig');
    }

    /**
     * @Route("/pages/new", name="pages_new")
     */
    public function newAction(Request $request)
    {
        $langs = $this->get('app.languages')
            ->getActiveLanguages();
        $Page = new Page();
        $form = $this->createForm(PageForm::class, $Page,
            ['langs' => $langs, 'layout' => $this->getParameter('generali')['layout']]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var Page
             */
            $Page = $form->getData();
            $em = $this->getDoctrine()
                ->getManager();
            $em->persist($Page);
            $em->flush();
            $translator = $this->get('translator');
            $rootDir = $this->get('app.web_dir')
                    ->get() . '/';
            if (!is_dir($rootDir . $Page->getUploadDir())) {
                mkdir($rootDir . $Page->getUploadDir(), 0755, true);
            }
            foreach ($Page->getPageRows() as $PageRow) {
                /*
                 * @var PageRows
                 */
                if ($PageRow->getSlot1Image()) {
                    $file = $rootDir . 'files/pages/' . $PageRow->getSlot1Image();
                    if (file_exists($file)) {
                        rename($file, $rootDir . $Page->getUploadDir() . $PageRow->getSlot1Image());
                    }
                }
                if ($PageRow->getSlot2Image()) {
                    $file = $rootDir . 'files/pages/' . $PageRow->getSlot2Image();
                    if (file_exists($file)) {
                        rename($file, $rootDir . $Page->getUploadDir() . $PageRow->getSlot2Image());
                    }
                }
                if ($PageRow->getSlot3Image()) {
                    $file = $rootDir . 'files/pages/' . $PageRow->getSlot3Image();
                    if (file_exists($file)) {
                        rename($file, $rootDir . $Page->getUploadDir() . $PageRow->getSlot3Image());
                    }
                }
            }
            $elemento = $Page->translate($request->getLocale())
                    ->getTitolo() . ' (' . $Page->getId() . ') ';
            $this->addFlash('success', 'Pagina ' . $elemento . $translator->trans('default.labels.creata'));

            return $this->redirectToRoute('pages_list');
        }
        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;

        return $this->render('admin/pages/new.html.twig',
            ['form' => $form->createView(), 'supportData' => $supportData]);
    }

    /**
     * Action che restitisce il json da mostrare in datatable.
     *
     * @Route("/pages/json", name="pages_list_json")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listJson(Request $request)
    {
        $canViewDeleted = false;
        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $canViewDeleted = true;
        }
        $return = [];

        try {
            $data = $this->get('app.services.json_list_helper')
                ->setCanViewDeleted($canViewDeleted)
                ->setEntity(Page::class)
                ->setFields([
                    'template',
                    ['titolo' => 'translations[it].titolo_menu'],
                    'updatedAt',
                    'createdAt',
                    ['slug' => 'translations[it].slug'],
                    ['genitore' => 'parent.translations[it].titolo_menu'],
                    'deleted',
                ])
                ->setModifier('slug', function ($value) {
                    if ('home' == $value) {
                        return '/';
                    }

                    return $value;
                })
                ->setModifier('parent', function ($value) {
                    if (!$value) {
                        $value = 'Livello base';
                    }

                    return $value;
                })
                ->getList();
            $return['result'] = true;
            $return['data'] = $data;
        } catch (\Exception $e) {
            $return['result'] = false;
            $return['errors'] = [$e->getMessage()];
        }

        return new JsonResponse($return);
    }

    /**
     * @Route("/pages/edit/{id}", name="pages_edit")
     */
    public function editAction(Request $request, Page $Page)
    {
        $em = $this->getDoctrine()
            ->getManager();
        /** ESPERIMENTO SU VERSIONAMENTO */
        /*
        $PageTranslations = $Page->getTranslations();
        $repo             = $em->getRepository('Gedmo\Loggable\Entity\LogEntry');
        foreach($PageTranslations as $pageTranslation) {
            $logs = $repo->getLogEntries($pageTranslation);

            dump($logs);

            if($logs) {

                $repo->revert($pageTranslation, 1);
                $em->persist($pageTranslation);

            }
        }

        $em->flush();
        */
        $originalPageRows = new ArrayCollection();
        // Crea an ArrayCollection delle attuali pagerows nel db
        foreach ($Page->getPageRows() as $pageRow) {
            $originalPageRows->add($pageRow);
        }
        $langs = $this->get('app.languages')
            ->getActiveLanguages();
        $form = $this->createForm(PageForm::class, $Page,
            ['langs' => $langs, 'layout' => $this->getParameter('generali')['layout']]);
        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;
        if ($Page->getListImgFileName()) {
            $supportData['listImgUrl'] = '/' . $Page->getUploadDir() . $Page->getListImgFileName();
        }
        if ($Page->getHeaderImgFileName()) {
            $supportData['headerImgUrl'] = '/' . $Page->getUploadDir() . $Page->getHeaderImgFileName();
        }
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($originalPageRows as $pageRow) {
                if (false === $Page->getPageRows()
                        ->contains($pageRow)) {
                    $em->remove($pageRow);
                }
            }
            $cancellaHeaderPrecedente = $request->get('page_form')['headerImgDelete'];
            if ($cancellaHeaderPrecedente) {
                $this->get('vich_uploader.upload_handler')
                    ->remove($Page, 'headerImg');
                $Page->setHeaderImg(null);
                $Page->setHeaderImgAlt('');
            }
            $cancellaListPrecedente = $request->get('page_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')
                    ->remove($Page, 'listImg');
                $Page->setListImg(null);
                $Page->setListImgAlt('');
            }
            $em = $this->getDoctrine()
                ->getManager();
            $em->persist($Page);
            $em->flush();
            $translator = $this->get('translator');

            return $this->redirectToRoute('pages_list');
        }

        return $this->render('admin/pages/edit.html.twig',
            ['form' => $form->createView(), 'supportData' => $supportData, 'page' => $Page]);
    }

    /**
     * @Route("/pages/delete/{id}/{force}", name="pages_delete",  requirements={"id" = "\d+"}, defaults={"force" =
     *     false}))
     */
    public function deleteAction(Request $request, Page $Page)
    {
        $elemento = $Page->translate('it')
                ->getTitolo() . ' (' . $Page->getId() . ')';
        $em = $this->getDoctrine()
            ->getManager();
        if ($Page->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && 1 == $request->get('force')) {
            // initiate an array for the removed listeners
            $originalEventListeners = [];
            // cycle through all registered event listeners
            foreach ($em->getEventManager()
                         ->getListeners() as $eventName => $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {
                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;
                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()
                            ->removeEventListener($eventName, $listener);
                    }
                }
            }
            // remove the entity
            $em->remove($Page);
            $em->flush();
        } elseif (!$Page->isDeleted()) {
            $em->remove($Page);
            $em->flush();
        }
        $this->addFlash('success', 'Pagina ' . $elemento . ' eliminata');

        return $this->redirectToRoute('pages_list');
    }

    /**
     * @Route("/pages/sort", name="pages_sort")
     */
    public function sortAction()
    {
        return $this->render('admin/pages/sort.html.twig', ['page_form' => '']);
    }

    /**
     * Action che salva l'ordinamento delle pagine quando questo avviene attraverso l'interfaccia
     * grafica di drag & drop fornita dal tree di jquery.
     *
     * @Route("/pages/save_json_tree")
     *
     * @param Request $request
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return JsonResponse
     */
    public function jsonTreeSaveAction(Request $request)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $pageHelper = $this->get('app.page_helper');
        $tree = $request->get('tree');
        $tree = json_decode($tree, true);
        if (isset($tree) && count($tree)) {
            foreach ($tree as $node) {
                $pageHelper->riordina($node);
            }
        }
        $return = [];
        $return['result'] = true;

        return new JsonResponse($return);
    }

    /**
     * @Route("/pages/json_tree")
     */
    public function jsonTreeAction()
    {
        $PageHelper = $this->get('app.page_helper');
        $p = new Page();
        $em = $this->getDoctrine()
            ->getManager();
        $AllPages = $em->getRepository('AppBundle:Page')
            ->findAll();
        $Pages = $em->getRepository('AppBundle:Page')
            ->findBy(['materializedPath' => '']);
        $all = [];
        $data = [];
        $data = ['text' => 'Radice', 'icon' => 'fa fa-folder', 'children' => []];
        foreach ($Pages as $Page) {
            $additionalData = ['icon' => 'fa fa-file-o', 'type' => 'default'];
            $data['children'][] = $PageHelper->jsonTree($Page, $AllPages, $additionalData);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/pages/restore/{id}", name="pages_restore")
     */
    public function restoreAction(Request $request, Page $Page)
    {
        if ($Page->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {
            $em = $this->getDoctrine()
                ->getManager();
            $elemento = $Page->translate('it')
                    ->getTitolo() . ' (' . $Page->getId() . ')';
            $Page->restore();
            $pageSlugger = $this->get('page_slugger');
            foreach ($Page->getTranslations() as $PageTranslation) {
                /*
                 * @var PageTranslation
                 */
                if ($PageTranslation->getIsEnabled()) {
                    $newSlug = $pageSlugger->slugify($PageTranslation, $PageTranslation->getSlug());
                    if ($PageTranslation->getSlug() != $newSlug) {
                        $PageTranslation->setSlug($newSlug);
                    }
                }
            }
            $em->flush();
            $this->addFlash('success', 'Pagina ' . $elemento . ' ripristinata');
        }

        return $this->redirectToRoute('pages_list');
    }
}
