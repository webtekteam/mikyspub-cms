<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Front;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/{_locale}/{slug}", requirements={"_locale" = "de|en|fr|jp|nl|pt|ru|zh|cs|pl|es", "slug"=".+"}, name="page")
     * @Route("/{slug}", defaults={"_locale"="it"}, requirements={"slug"=".+"}, name="page_it")
     */
    public function pageAction(Request $request)
    {
        if ($request->attributes->has('slug')) {
            if ('home' == $request->get('slug')) {
                $route = 'home';

                if ('it' == $request->getLocale()) {
                    $route .= '_it';
                }

                return $this->redirectToRoute($route);
            }

            $seo_manager = $this->get('app.seo-manager');

            $pageRetriever = $this->get('app.page_retriever');

            /**
             * var Page $page.
             */
            $page = $pageRetriever->retrievePage($request->get('slug'), $request->getLocale());

            if ($page) {
                $Languages = $this->get('app.languages');

                $TemplateLoader = $this->get('app.template_loader');

                $AdditionalData = [];
                $AdditionalData['Entity'] = $page;
                $AdditionalData['langs'] = $Languages->getActivePublicLanguages();

                $meta = $seo_manager->run($page);

                $AdditionalData['META'] = $meta;

                $twigs = $TemplateLoader->getTwigs($page->getTemplate(), $Languages->getActivePublicLanguages(),
                    $AdditionalData);

                return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $meta]);
            }
        }

        throw new NotFoundHttpException('Pagina non trovata');
    }
}
