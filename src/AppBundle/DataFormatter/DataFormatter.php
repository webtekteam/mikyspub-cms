<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class DataFormatter
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $data;

    protected $locale;

    protected $AdditionalData;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(ContainerInterface $container, Request $request, $data = [], $AdditionalData = null)
    {
        $this->container = $container;
        $this->request = $request;
        $this->AdditionalData = $AdditionalData;
        $this->em = $container->get('doctrine')
            ->getManager();
        $this->data = $data;
        $this->locale = $this->request->getLocale();

        $this->extractData();
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function setWidgetConfig($config)
    {
        $this->data = $config;
    }

    public function setAdditionalData($AdditionalData)
    {
        $this->AdditionalData = $AdditionalData;
    }

    abstract protected function getData();

    abstract protected function extractData();

    /**
     * Restituisce un parametro del modulo.
     *
     * ##NON FUNZIONA CON I PARAMETRI ALLOW_ADD##
     * Restituisce tra i parametri del modulo, quello con nome passato come primo parametro, nel locale corrente;
     *
     * Se questo non è stato trovato, verrà restituito il valore passato come secondo parametro (opzionale)
     *
     * &nbsp;
     *
     * EG:
     *
     * getParameter("testo_bottone") equivale a
     * $this->data[$this->locale]["testo_bottone"]["val"]; null se questo non è impostato
     *
     * getParameter("livello", 10) equivale a
     * $this->data[$this->locale]["livello"]["val"]; 10 se questo non è impostato
     *
     * @param string $key Campo "var" del parametro interessato
     * @param mixed $default [opzionale] Valore di default, nel caso in cui il parametro interessato non si presente
     *
     * @return mixed
     */
    protected function getParameter($key, $default = null)
    {
        $toRet = $default;

        if (array_key_exists($this->locale, $this->data)) {
            if (array_key_exists($key, $this->data[$this->locale])) {
                $toRet = $this->data[$this->locale][$key]['val'];
            }
        }

        return $toRet;
    }
}
