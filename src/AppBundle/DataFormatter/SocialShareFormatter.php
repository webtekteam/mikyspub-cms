<?php
// 13/02/17, 17.12
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppBundle\Entity\Page;
use Mremi\UrlShortenerBundle\Tests\Entity\Link;

class SocialShareFormatter extends DataFormatter
{

    public function getData()
    {

        $request = $this->request;

        $attributes = $request->attributes->all();

        $data = [];

        if (isset($attributes['_route'])) {

            $route = preg_replace('/_it$/', '', $attributes['_route']);

            switch ($route) {

                case 'blog_read_news':

                    /**
                     * @var $News News
                     */
                    $News = $this->AdditionalData['Entity'];

                    $data = [];
                    $data['twitter']['url'] = $News->translate($this->locale)->getShortUrl();
                    $data['twitter']['text'] = substr($News->translate($this->locale)->getTitolo(), 0, 120);
                    if (isset($this->AdditionalData['META']) && isset($this->AdditionalData['META']['social']) && isset($this->AdditionalData['META']['social']['twitter']) && isset($this->AdditionalData['META']['social']['twitter']['site']) && $this->AdditionalData['META']['social']['twitter']['site']) {
                        $data['twitter']['via'] = $this->AdditionalData['META']['social']['twitter']['site'];
                    }

                    $data['linkedin']['url'] = $data['twitter']['url'];
                    $data['linkedin']['title'] = substr($News->translate($this->locale)->getTitolo(), 0, 200);
                    $data['linkedin']['summary'] = $News->translate($request->getLocale())->getMetaDescription();


                    $data['gplus']['url'] = $data['twitter']['url'];
                    $data['gplus']['title'] = substr($News->translate($this->locale)->getTitolo(), 0, 200);
                    $data['gplus']['summary'] = $News->translate($request->getLocale())->getMetaDescription();

                    $data['pinterest']['url'] = $data['twitter']['url'];
                    $data['pinterest']['title'] = substr($News->translate($this->locale)->getTitolo(), 0, 200);
                    $data['pinterest']['summary'] = $News->translate($request->getLocale())->getMetaDescription();
                    $data['pinterest']['media'] = $this->AdditionalData['META']['social']['og']['image'];

                    break;

                case 'page':

                    /**
                     * @var $Page Page
                     */
                    $Page = $this->AdditionalData['Entity'];

                    if (!$Page->translate($this->locale)->getShortUrl()) {

                        $link = new Link();
                        $link->setLongUrl($request->getUri());


                        $chainProvider = $this->container->get('mremi_url_shortener.chain_provider');

                        $chainProvider->getProvider('bitly')->shorten($link);

                        $Page->translate($request->getLocale())->setShortUrl($link->getShortUrl());

                        $this->em->persist($Page);
                        $this->em->flush();

                    }

                    $data = [];
                    $data['twitter']['url'] = $Page->translate($this->locale)->getShortUrl();
                    $data['twitter']['text'] = substr($Page->translate($this->locale)->getTitolo(), 0, 120);
                    if (isset($this->AdditionalData['META']) && isset($this->AdditionalData['META']['social']) && isset($this->AdditionalData['META']['social']['twitter']) && isset($this->AdditionalData['META']['social']['twitter']['site']) && $this->AdditionalData['META']['social']['twitter']['site']) {
                        $data['twitter']['via'] = $this->AdditionalData['META']['social']['twitter']['site'];
                    }

                    $data['linkedin']['url'] = $data['twitter']['url'];
                    $data['linkedin']['title'] = substr($Page->translate($this->locale)->getTitolo(), 0, 200);
                    $data['linkedin']['summary'] = $Page->translate($request->getLocale())->getMetaDescription();

                    break;


            }

        }

        return $data;
    }

    public function extractData()
    {
        // TODO: Implement extractData() method.
    }

}