<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;

use AppBundle\Entity\Page;
use AppBundle\Entity\PageRows;

class PageRowsFormatter extends DataFormatter
{
    public function extractData()
    {
        // TODO: Implement getData() method.
    }

    public function getRows($da = 0, $a = 3)
    {
        /**
         * @var Page
         */
        $Page = $this->AdditionalData['Entity'];

        $rows = [];

        if ($Page && $Page instanceof Page) {
            $out = '';

            foreach ($Page->getPageRows() as $PageRow) {
                /**
                 * @var PageRows
                 */
                if ($PageRow->getLocale() == $this->locale) {
                    $rows[] = $PageRow;
                }
            }

            $rows = array_slice($rows, 0, 3);
        }

        $data = [];
        $data['rows'] = $rows;

        return $data;
    }

    public function getData()
    {
        /**
         * @var Page
         */
        $Page = $this->AdditionalData['Entity'];

        $rows = [];

        if ($Page) {
            $primaRiga = false;
            if ($this->data[$this->locale]['da_riga']['val']) {
                $primaRiga = (int)($this->data[$this->locale]['da_riga']['val'] - 1);
            }
            $length = null;

            if (is_numeric($this->data[$this->locale]['a_riga']['val']) && $this->data[$this->locale]['a_riga']['val'] > $primaRiga) {
                $length = $this->data[$this->locale]['a_riga']['val'] - $primaRiga;
            }

            $out = '';

            foreach ($Page->getPageRows() as $PageRow) {
                /**
                 * @var PageRows
                 */
                if ($PageRow->getLocale() == $this->locale) {
                    $rows[] = $PageRow;
                }
            }

            if (false !== $primaRiga) {
                $rows = array_slice($rows, $primaRiga, $length);
            }
        }

        $data = [];
        $data['rows'] = $rows;

        return $data;
    }

    public function getRow()
    {
        $numero_riga = (int)($this->getParameter('numero_riga', 1)) - 1;

        if ($numero_riga < 0) {
            return ['error' => 'Il numero della riga richiesto non può essere < 0'];
        }

        $row = $this->em->getRepository('AppBundle:PageRows')
            ->findOneByPageAndIndex($this->AdditionalData['Entity'], $this->locale, $numero_riga);

        if (null === $row) {
            return ['error' => 'La riga con indice "' . ($numero_riga + 1) . '" non è stata trovata'];
        }

        return $row;
    }
}
