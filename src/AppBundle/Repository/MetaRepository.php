<?php
// 24/05/17, 14.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class MetaRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('n')
            ->andWhere('n.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

}