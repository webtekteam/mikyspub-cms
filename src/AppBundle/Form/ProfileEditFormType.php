<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileEditFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome', null, ['label' => 'operatori.labels.nome'])
            ->add('cognome', null, ['label' => 'operatori.labels.cognome'])
            ->add('email', EmailType::class, ['label' => 'operatori.labels.email'])
            ->add('username', null, ['disabled' => true, 'required' => false, 'label' => 'operatori.labels.username'])
            ->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'required' => false,
                    'first_options' => ['label' => 'operatori.labels.password', 'help' => 'operatori.help.password'],
                    'second_options' => ['label' => 'operatori.labels.repeat_password'],
                ]
            )
            ->add(
                'profileText',
                TextareaType::class,
                [
                    'label' => 'operatori.labels.profile_text',
                    'required' => false,
                ]
            );

        $builder->add('listImg', FileType::class, ['label' => false, 'attr' => ['class' => 'upload-trigger']]);
        $builder->add('listImgData', HiddenType::class, ['attr' => ['class' => 'data-holder']]);
        $builder->add('listImgDelete', HiddenType::class, ['attr' => ['class' => 'delete-control']]);

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
                'allow_extra_fields' => true,
            ]
        );

    }

}