<?php
// 12/06/17, 16.33
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\EmailTemplate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailTemplateForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $data = $builder->getData();

        $builder->add(
            'codice',
            TextType::class,
            [
                'label' => 'email_template.labels.codice',
                'required' => true,
                'attr' => ['readonly' => ($data->getId()) ? true : false],
            ]
        );

        $builder->add(
            'variabili',
            TextType::class,
            [
                'label' => 'email_template.labels.variabili',
                'required' => false,
            ]
        );

        $fields = [
            'subject' => [
                'label' => 'email_template.labels.subject',
                'required' => true,
            ],
            'text' => [
                'label' => 'email_template.labels.testo',
                'required' => true,
                'field_type' => TextareaType::class,
                'attr' => ['class' => 'ck'],
            ],

        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => EmailTemplate::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}