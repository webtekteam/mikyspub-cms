<?php
// 13/08/17, 6.51
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use AppBundle\Entity\Group;
use AppBundle\Entity\GroupTypes;
use AppBundle\Entity\MappedRoles;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', TextType::class, ['label' => 'groups.labels.name']);

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add(
            'roles',

            EntityType::class,
            [
                'label' => 'group.labels.roles',
                'class' => MappedRoles::class,
                'multiple' => true,
                'attr' => ['size' => 50],
//                'expanded' => true,
                'group_by' => function ($val, $key, $index) {
                    /**
                     * @var $val MappedRoles
                     */

                    return (string)$val->getGroupType();
                },
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Group::class,
                'error_bubbling' => true,
            ]
        );
    }


}