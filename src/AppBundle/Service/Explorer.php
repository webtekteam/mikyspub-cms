<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace AppBundle\Service;

use Symfony\Component\Finder\Finder;

/**
 * Classe di utility per reperire l'elenco di files presenti in un path relativo alla root dir del progetto.
 *
 * Class Explorer
 */
class Explorer
{
    /**
     * @var string
     */
    private $rootDir;

    /**
     * Explorer constructor.
     * Tramite configurazione nel file services.yml riceve la root dir reperita da symfony.
     *
     * @param string $rootDir Directory di root del progetto
     */
    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * Ritorna la stringa relativa alla root del progetto.
     *
     * @return string $rootDir
     */
    private function getRootDir()
    {
        return $this->rootDir;
    }

    /**
     * Ritorna un'array di files che soddisfano i filtri di ricerca passati al metodo
     *
     * @param string $relPath Percorso relativo alla root di progetto
     * @param mixed $extension Parametro opzionale che permette di estrarre solo i file con l'estensione indicata
     *
     * @return array
     */
    public function listFiles(string $relPath, $extension = null)
    {
        $finder = new Finder();
        $finder = $finder->files()->in($this->rootDir . '/' . $relPath);
        if ($extension) {
            $finder = $finder->name('*' . $extension);
        }
        $data = [];
        foreach ($finder as $file) {
            $name = basename($file->getRealPath());
            if ($extension) {
                $name = str_replace($extension, '', $name);
            }
            $data[$name] = $name;
        }
        asort($data);

        return $data;
    }
}
