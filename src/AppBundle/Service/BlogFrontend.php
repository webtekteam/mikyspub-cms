<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;

use AppBundle\Entity\News;
use AppBundle\Entity\NewsRows;
use AppBundle\Entity\NewsTranslation;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BlogFrontend
{
    /**
     * @var Translator
     */
    private $translator;
    /**
     * @var EngineInterface
     */
    private $twig;
    private $layout;

    /**
     * @var EntityManager
     */
    private $em;
    /** @var string $locale La lingua nella quale si sta navigando */
    private $locale;

    /**
     * BlogFrontend constructor.
     *
     * @param ContainerInterface $container
     * @param Languages $languages
     */
    public function __construct(ContainerInterface $container, Languages $languages)
    {
        $this->translator = $container->get('translator');
        $this->twig = $container->get('twig');
        $this->layout = $container->getParameter('generali')['layout'];

        $this->em = $container->get('doctrine')
            ->getManager();
        $this->locale = $languages->getLanguage()
            ->getLocale();
    }

    public function getTextPreview(News $News, $length = 50)
    {
        /**
         * @var $newsRow NewsRows
         */
        foreach ($News->getNewsRows() as $newsRow) {
            if ($newsRow->getLocale() == $this->locale) {
                $struttura = $newsRow->getStructure();
                $struttura = json_decode($struttura, true);

                foreach ($struttura as $slot => $data) {
                    if ('ck' == $data['type']) {
                        $method = 'getSlot' . ($slot + 1);
                        $text = strip_tags($newsRow->$method());

                        if (strlen($text) > $length) {
                            $text = substr(html_entity_decode($text, ENT_QUOTES, 'UTF-8'), 0, $length) . '...';
                        }

                        return $text;
                    }
                }
            }
        }

        return 'Sunt capioes visum grandis, germanus historiaes.';
    }

    /**
     * @param News $News
     * @param int $length
     *
     * @return string
     */
    public function getTextPreviewRaw(News $News, $length = 50)
    {
        /**
         * @var $newsRow NewsRows
         */
        foreach ($News->getNewsRows() as $newsRow) {
            if ($newsRow->getLocale() == $this->locale) {
                $struttura = $newsRow->getStructure();

                $struttura = json_decode($struttura, true);

                foreach ($struttura as $slot => $data) {
                    if ('ck' == $data['type']) {
                        $method = 'getSlot' . ($slot + 1);
                        $text = $newsRow->$method();

                        if (strlen($text) > $length) {
                            $text = substr(html_entity_decode($text, ENT_QUOTES, 'UTF-8'), 0, $length) . '...';
                        }

                        return $text;
                    }
                }
            }
        }

        return 'Sunt capioes visum grandis, germanus historiaes.';
    }

    public function getListImg(News $news, $attribute = 'src')
    {
        /** @var NewsTranslation $news_translation */
        $news_translation = $news->translate($this->locale);

        if ($news->getListImgFileName()) {
            $img = [
                'alt' => $news->getListImgAlt(),
                'src' => '/' . $news->getUploadDir() . '/' . $news->getListImgFileName(),
            ];
        } else {
            $img = [
                'alt' => $news_translation->getTitolo(),
                'src' => '/images/loghi/placeholder.png',
            ];
        }

        return $img[$attribute];
    }

    public function getCategory(News $News)
    {
        return $News->getPrimaryCategory()
            ->translate($this->locale);
    }

    /**
     * @param News $News
     *
     * @return array|mixed attachments
     */
    public function getAttachments(News $News)
    {
        return $News->getAttachments();
    }

    /**
     * @param News $News
     * @param string $categoryToCheck
     *
     * @return bool as true when News belongs to a specific category
     */
    public function newsInCategory(News $News, $categoryToCheck = 'default')
    {
        if ($News) {
            $categories = $this->getAllCategories($News);
            foreach ($categories as $category) {
                if ($category->getSlug() == $categoryToCheck) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param News $News
     *
     * @return array with primary category and related
     */
    public function getAllCategories(News $News)
    {
        $categories = [];

        if ($News) {
            array_push($categories, $News->getPrimaryCategory()
                ->translate($this->locale));
            foreach ($News->getCategorieAggiuntive() as $singleCategory) {
                array_push($categories, $singleCategory->translate($this->locale));
            }
        }

        return $categories;
    }

    public function getDate($Entity, $field = 'publishAt')
    {
        $method = 'get' . ucfirst($field);

        /** @var DateTime $date */
        $date = $Entity->$method();

        $dataFormattata = $this->translator->trans('cal.labels.giorno_' . $date->format('N'));
        $dataFormattata .= ' ';
        $dataFormattata .= $date->format('d');
        $dataFormattata .= ' ';
        $dataFormattata .= $this->translator->trans('cal.labels.mese_' . $date->format('n'));
        $dataFormattata .= ' ';
        $dataFormattata .= $date->format('Y');

        return $dataFormattata;
    }

    public function getDateFormat($Entity, $format = 'd-m-Y', $field = 'publishAt')
    {
        $method = 'get' . ucfirst($field);

        /** @var DateTime $date */
        $date = $Entity->$method();

        if ('%' == $format[0]) {
            switch ($format[1]) {
                case 'b':
                    $dataFormattata = $this->translator->trans('cal.labels.mese_short_' . $date->format('n'));

                    break;
                case 'B':
                    $dataFormattata = $this->translator->trans('cal.labels.mese_' . $date->format('n'));

                    break;
                default:
                    $dataFormattata = $format;
            }
        } else {
            $dataFormattata = $date->format($format);
        }

        return $dataFormattata;
    }

    public function getNewsText($rows)
    {
        $out = '';

        foreach ($rows as $row) {
            /**
             * @var $row NewsRows
             */
            if ($row->getLocale() == $this->locale) {
                $out .= $this->getRow($row);
            }
        }

        return $out;
    }

    public function getRow($row)
    {
        return $this->twig->render('public/layouts/' . $this->layout . '/includes/news-row.twig', ['row' => $row]);
    }

    public function getCorrelate(News $news, $quante = 2)
    {
        $category = $news->getPrimaryCategory();

        $NewsList = $this->em->getRepository('AppBundle:News')
            ->getCorrelate($news, $category, $quante);

        return $NewsList;
    }

    public function getText(NewsRows $row, $slotNumber = 1)
    {
        $method = 'getSlot' . $slotNumber;

        $re = '/\[youtube(.*)\]/';

        $text = $row->$method();

        preg_match_all($re, $text, $m);

        if ($m) {
            foreach ($m[1] as $k => $html) {
                $rawHtml = html_entity_decode($html, ENT_QUOTES, 'UTF-8');

                $reUrl = '/url="([^"]+)"/';

                preg_match($reUrl, $rawHtml, $mUrl);

                $data = [];
                $data['code'] = false;
                $data['caption'] = false;

                if ($mUrl) {
                    $youtubeData['code'] = trim(parse_url($mUrl[1], PHP_URL_PATH), '/');

                    $reCaption = '/caption="([^"]+)"/';

                    preg_match($reCaption, $rawHtml, $mCaption);

                    if ($mCaption) {
                        $youtubeData['caption'] = $mCaption[1];
                    }

                    $text = str_replace($m[0][$k],
                        $this->twig->render('public/layouts/' . $this->layout . '/includes/youtube.twig',
                            ['youtubeData' => $youtubeData]), $text);
                }
            }
        }

        return $text;
    }

    public function getCountCommenti(News $News)
    {
        return $this->em->getRepository('AppBundle:Commento')
            ->getCommentiCountForNews($News);
    }
}
