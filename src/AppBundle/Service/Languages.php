<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\Service;

use AppBundle\Entity\Language;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class Languages
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    private $languages;

    private $publicLanguages;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Language
     */
    private $currentLanguage;

    public function __construct(Container $container, EntityManager $em, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->em = $em;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getActiveLanguages()
    {
        if (!$this->languages) {
            $this->languages = $this->em->getRepository('AppBundle:Language')
                ->getBackendLanguages();
        }

        $data = [];

        foreach ($this->languages as $Language) {
            /*
             * @var $Language Language
             */
            $data[$Language->getShortCode()] = $Language->getLanguageName();
        }

        return $data;
    }

    public function getActivePublicLanguages()
    {
        if (!$this->publicLanguages) {
            $this->publicLanguages = $this->em->getRepository('AppBundle:Language')
                ->getPublicLanguages();
        }

        $data = [];

        foreach ($this->publicLanguages as $Language) {
            /*
             * @var $Language Language
             */
            $data[$Language->getShortCode()] = $Language->getLanguageName();
        }

        return $data;
    }

    public function getActivePublicLanguagesKeys()
    {
        return array_keys($this->getActivePublicLanguages());
    }

    public function getLanguage()
    {
        if (!$this->currentLanguage) {
            $locale = $this->request->getLocale();

            $this->currentLanguage = $this->em->getRepository('AppBundle:Language')
                ->findOneBy(['shortCode' => $locale]);

            if (null === $this->currentLanguage) {
                throw new Exception('La lingua ' . $locale . ' non esiste');
            }
        }

        return $this->currentLanguage;
    }

    public function getLanguageLocale()
    {
        $this->getLanguage();

        return str_replace('_', '-', $this->currentLanguage->getLocale());
    }

    /**
     * Restituisce l'ordine desiderato di hreflang, come specificato dall'utente.
     *
     * @return string[]
     */
    public function getHrefLangOrder()
    {
        // Ottiene gli hreflang in ordine come specificati dall'utente
        $href_langs = $this->getHrefLangList();

        // Filtra solo le lingue attive
        $available_locales = $this->getActivePublicLanguagesKeys();
        $href_langs = $this->filterHrefLangs($href_langs, $available_locales);

        // Accoda tutte le lingue attive, ma non incluse
        $excluded_locales = $this->getExcludedFromHreflang($available_locales, $href_langs);
        $href_langs = array_merge($href_langs, $excluded_locales);

        return $href_langs;
    }

    //region HrefLangs

    /**
     * Restituisce una lista di tutti gli elementi di $source che siano presenti anche in $filter.
     *
     * @param string[] $source
     * @param string[] $filter
     *
     * @return string[]
     */
    private function filterHrefLangs($source, $filter)
    {
        $toRet = [];

        foreach ($source as $lang) {
            if (in_array($lang, $filter) && !in_array($lang, $toRet)) {
                $toRet[] = $lang;
            }
        }

        return $toRet;
    }

    /**
     * Restituisce una lista di tutte le lingue attive non incluse nell'ordine degli hreflangs.
     *
     * @param string[] $all_locales
     * @param string[] $href_langs
     *
     * @return string[]
     */
    private function getExcludedFromHreflang($all_locales, $href_langs)
    {
        $toRet = [];

        foreach ($all_locales as $locale) {
            if (!in_array($locale, $href_langs)) {
                $toRet[] = $locale;
            }
        }

        return $toRet;
    }

    /**
     * Ottiene la lista di hreflang come specificati in webtek-new.
     *
     * @return string[]
     */
    private function getHrefLangList()
    {
        $langs = $this->container->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $langs = preg_split('/ *, */', $langs);

        return $langs;
    }

    //endregion
}
