<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;

use AppBundle\Entity\Group;
use AppBundle\Entity\MappedRoles;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Yaml;

/**
 * Class GroupHelper.
 */
class GroupHelper
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var mixed
     */
    private $rootDir;
    /**
     * @var string
     */
    private $securityFile;

    /**
     * AttributesHelper constructor.
     *
     * @param EntityManager $entityManager
     * @param mixed $rootDir
     */
    public function __construct(EntityManager $entityManager, $rootDir)
    {
        $this->entityManager = $entityManager;
        $this->rootDir = $rootDir;
        $this->securityFile = $this->rootDir . '/config/security.yml';
    }

    /**
     *  Scrive il file security aggiornandolo con tutti i roles definiti al salvataggio di un gruppo
     */
    public function writeSecurity()
    {
        $data = $this->readYamlFile();
        $RoleHierarchy = $this->generateRoleYerarchy();
        $data['security']['role_hierarchy'] = $RoleHierarchy;
        file_put_contents($this->securityFile, Yaml::dump($data, 3));
    }

    /**
     * Legge il file security.yml
     *
     * @return bool|mixed
     */
    private function readYamlFile()
    {
        $path = $this->securityFile;
        if (file_exists($path)) {
            $data = Yaml::parse(file_get_contents($path));

            return $data;
        }

        return false;
    }

    /**
     * Ritorna la gerarchia di tutti i roles per ogni gruppo.
     *
     * @return array
     */
    private function generateRoleYerarchy()
    {
        $RoleHierarchy = [];
        $MappedRoles = $this->entityManager->getRepository('AppBundle:MappedRoles')->findAll();
        $RoleHierarchy['ROLE_SUPER_ADMIN'] = [];
        foreach ($MappedRoles as $mappedRole) {
            $RoleHierarchy['ROLE_SUPER_ADMIN'][] = $mappedRole->getRole();
        }
        $Groups = $this->entityManager->getRepository('AppBundle:Group')->findAll();
        foreach ($Groups as $group) {
            $RoleHierarchy[$group->getRole()] = [];
            foreach ($group->getRoles() as $Role) {
                /*
                 * @var $Role MappedRoles
                 */
                $RoleHierarchy[$group->getRole()][] = $Role->getRole();
            }
        }

        return $RoleHierarchy;
    }
}
