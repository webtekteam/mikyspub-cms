<?php

namespace AppBundle\Service;


use AppBundle\Entity\GalleryCategory;
use Doctrine\ORM\EntityManager;

class GalleryCatHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * ListinosHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Categories = $this->entityManager->getRepository('AppBundle:GalleryCategory')->findBy(
                [],
                ['sort' => 'ASC']
            );
        } else {
            $Categories = $this->entityManager->getRepository('AppBundle:GalleryCategory')->findAllNotDeleted();
        }

        $records = [];

        foreach ($Categories as $Category) {


            /**
             * @var $Category GalleryCategory;
             */

            $record = [];
            $record['id'] = $Category->getId();
            $record['titolo'] = $Category->translate()->getTitolo();
            $record['template'] = $Category->getTemplate();
            $record['isEnabled'] = $Category->getIsEnabled();
            $record['deleted'] = $Category->isDeleted();
            $record['createdAt'] = $Category->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Category->getUpdatedAt()->format('d/m/Y H:i:s');


            $records[] = $record;
        }

        return $records;

    }


}