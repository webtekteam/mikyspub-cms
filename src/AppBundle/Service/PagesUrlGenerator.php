<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace AppBundle\Service;

use AppBundle\Entity\Page;
use AppBundle\Entity\PageTranslation;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PagesUrlGenerator.
 *
 * @author    Batman<bat.man@nanana.bat>
 * @author    Vkfan<class-dead@hotmail.com>
 * @copyright Webtek S.p.A. 2018
 *
 * @version   1.0.0
 */
class PagesUrlGenerator
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var Request
     */
    private $request;

    /**
     * PagesUrlGenerator constructor.
     *
     * @param EntityManager $em
     * @param Request|RequestStack $request
     */
    public function __construct(EntityManager $em, $request)
    {
        $this->em = $em;

        if ($request instanceof RequestStack) {
            $this->request = $request->getCurrentRequest();
        } else {
            $this->request = $request;
        }
    }

    /**
     * Genera un url partendo da una Page e dal locale.
     *
     * @param Page $page Pagina da linkare
     * @param string $locale Default it
     * @param bool $absolute Indica se aggiungere o meno l'HTTP_HOST
     *
     * @return string
     */
    public function generaUrl(Page $page, $locale = 'it', $absolute = false)
    {
        $path = '';
        /** @var PageTranslation $p_t */
        $p_t = $page->translate($locale);

        if ($absolute) {
            $path = $this->request->getSchemeAndHttpHost();
        }

        $slugs = [];
        $slug = $p_t->getSlug();

        if ('home' != $slug) {
            $slugs[] = $slug;
        }

        $parentId = $page->getParent();
        while ($parentId) {
            /**
             * var $parent Page;.
             */
            $page = $this->em->getRepository('AppBundle:Page')
                ->findOneBy(['id' => $parentId]);
            $p_t = $page->translate($locale);

            $slugs[] = $p_t->getSlug();

            $parentId = $page->getParent();
        }

        $slugs = array_reverse($slugs);
        $slugs = implode('/', $slugs);

        if ($locale && 'it' != $locale) {
            $slugs_locale = $locale;

            if ($slugs) {
                $slugs = $slugs_locale . '/' . $slugs;
            }
        }

        return $path . '/' . $slugs;
    }

    /**
     * @param string $template Template della pagina da linkare
     * @param null|string $locale Locale nel quale linkare la pagina; default quello della request
     * @param bool $absolute Indica se creare un url assoluto o relativo
     *
     * @return string
     */
    public function generateByTemplate($template, $locale = null, $absolute = false)
    {
        if (null === $locale) {
            $locale = $this->request->getLocale();
        }

        $page = $this->em->getRepository('AppBundle:Page')
            ->findOneByTemplate($template, $locale);

        return $this->generaUrl($page, $locale, $absolute);
    }
}
