<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:php:fiz')
            ->setDescription('Tool per fizzare il php');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $root_dir = __DIR__ . '/../../..';

        $command_output = shell_exec('git status | grep " .*\\.php$"');
        $command_output = explode("\n", $command_output);

        $files = [];
        foreach ($command_output as $f) {
            preg_match('/ ([^ ]*)$/', $f, $file_name);
            if (0 !== count($file_name)) {
                $file_name = trim($file_name[1]);
                $files[] = $file_name;
            }
        }

        // Prende l'eseguibile di php-cs-fixer
        $command_output = trim(shell_exec('which php-cs-fixer'));
        if ('' === $command_output) {
            $output->writeln('php-cs-fixer non trovato');

            return;
        }

        $fixer_path = $command_output;

        foreach ($files as $f) {
            shell_exec($fixer_path . ' fix --quiet --config=' . $root_dir . '/php_cs.dist ' . $f);
        }
    }
}
