# Webtek CMS

## Suggerimenti/TODOs
- Unificare lo slugger
- Ripulire tutti i servizi inutili
- Rifattorizzare i servizi in modo che si possano eliminare i bundle (notoriamente quello dell'ecommerce) non utilizzati senza intaccare l'intera struttura del cms

## Changelog
### [0.10.2] - TBD
- Alcuni tipi di variabili (backgroundImage, boolean, file, image mediumText, simpleText, simpleTextarea) possono essere raggruppati per convenienza
- Corretto formatting VetrinaFormatter
- Di default, gni prodotto ha la quantità di copie vendute impostata a 0
- La feature e le categorie dei prodotti hanno un codice breve indipendente dalla lingua (non obbligatorio)

### [0.10.1] - 2018-07-05
- Riformattato il mondo
- Aggiunto il readme

### [0.10.0] - 2018-07-04
- La lingua viene presa in automatico quando si usano gli helper di pagine e blog
- Passaggio a bootstrap 4